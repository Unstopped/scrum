<?Php

$heredoc = <<<heredoc
Het verschil tussen een Lokale en Globale variabele is dat je een globale variabele over je hele code gebruikt-
kan worden en een lokale variabele alleen in een bepaald stukje.

Voorbeeld: global \$myVariable;
Uitkomst: de variabele "\$myVariable" is nu globaal beschikbaar over je code.
heredoc;

echo $heredoc;
?>
