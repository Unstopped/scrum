SELECT 
dbo.tblContinent.ContinentName, 
dbo.tblContinent.Summary,
ISNULL(dbo.tblContinent.Summary,'No Summary') AS 'Using IsNull',
COALESCE(dbo.tblContinent.Summary,'No Summary') AS 'Using COALESCE',
CASE
	WHEN dbo.tblContinent.Summary IS NULL THEN 'No Summary'
	ELSE dbo.tblContinent.Summary
END AS 'Using CASE'
FROM 
dbo.tblContinent