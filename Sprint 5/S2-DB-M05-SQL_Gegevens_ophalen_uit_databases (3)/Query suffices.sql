SELECT 
dbo.tblEvent.EventName,  
FORMAT(dbo.tblEvent.EventDate, 'dddd') + ' ' 
+ CONVERT(varchar(10), DAY(dbo.tblEvent.EventDate)) + 'th ' 
+ CASE
	WHEN MONTH(dbo.tblEvent.EventDate) = 1 THEN 'January'
	WHEN MONTH(dbo.tblEvent.EventDate) = 2 THEN 'February'
	WHEN MONTH(dbo.tblEvent.EventDate) = 3 THEN 'March'
	WHEN MONTH(dbo.tblEvent.EventDate) = 4 THEN 'April'
	WHEN MONTH(dbo.tblEvent.EventDate) = 5 THEN 'May'
	WHEN MONTH(dbo.tblEvent.EventDate) = 6 THEN 'June'
	WHEN MONTH(dbo.tblEvent.EventDate) = 7 THEN 'July'
	WHEN MONTH(dbo.tblEvent.EventDate) = 8 THEN 'August'
	WHEN MONTH(dbo.tblEvent.EventDate) = 9 THEN 'September'
	WHEN MONTH(dbo.tblEvent.EventDate) = 10 THEN 'October'
	WHEN MONTH(dbo.tblEvent.EventDate) = 11 THEN 'November'
	WHEN MONTH(dbo.tblEvent.EventDate) = 12 THEN 'December'
	ELSE 'ERROR'
END + ' '
+ CONVERT(varchar(10), YEAR(dbo.tblEvent.EventDate))
AS 'Full date'
FROM 
dbo.tblEvent
ORDER BY
dbo.tblEvent.EventDate
ASC