function exchange(bedrag, conversie)
{
  var bedrag = arguments[0];
  var conversie = arguments[1];
  var result = 0;

  switch (conversie) {
    case "dollar/euro":
      result = bedrag * 0.74;
      document.write("<br>" + bedrag + " dollar is " + result.toFixed(2) + " euro");
      break;

    case "euro/dollar":
      result = bedrag * 1.36;
      document.write("<br>" + bedrag + " euro is " + result.toFixed(2) + " dollar");
      break;
    case "ruble/euro":
      result = bedrag * 0.02;
      document.write("<br>" + bedrag + " ruble is " + result.toFixed(2) + " euro");
      break;
    case "euro/ruble":
      result = bedrag * 48.40;
      document.write("<br>" + bedrag + " euro is " + result.toFixed(2) + " ruble");
      break;
    default:
    break;

  }
}
