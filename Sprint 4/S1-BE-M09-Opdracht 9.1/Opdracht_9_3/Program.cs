using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Opdracht_9_5
{
    class random
    {
        static int locationblockX = 1;
        static int locationblockY = 1;

        static void Main(string[] args)
        {
            //maak een random aan voor de locatie
            Random rnd = new Random();
            for (int i =0; i< 600; i++)
            {
                locationblockX = rnd.Next(1, 100);
                locationblockY = rnd.Next(1, 50);
                //maak het blokje dat gaat bewegen
                Console.SetCursorPosition(locationblockX, locationblockY);
                Console.WriteLine("*****");
                Console.SetCursorPosition(locationblockX, locationblockY+1);
                Console.WriteLine("*****");
                Console.SetCursorPosition(locationblockX, locationblockY+2);
                Console.WriteLine("*****");
                Thread.Sleep(500);
                //doe i - 1 zodat hij voor altijd 0  blijft
                i = i - 1;
                Console.Clear();

                //doe nu nog een random voor de kleur
                //Edited by Vincent on 3/5/2019
                int colour = rnd.Next(7);
                switch (colour)
                {
                    case 1:
                        Console.ForegroundColor = ConsoleColor.Gray;
                        break;
                    case 2:
                        Console.ForegroundColor = ConsoleColor.Red;
                        break;
                    case 3:
                        Console.ForegroundColor = ConsoleColor.Green;
                        break;
                    case 4:
                        Console.ForegroundColor = ConsoleColor.Blue;
                        break;
                    case 5:
                        Console.ForegroundColor = ConsoleColor.Magenta;
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        break;
                }
            }
        }
    }
}
