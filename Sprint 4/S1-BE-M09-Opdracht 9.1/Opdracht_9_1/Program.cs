//Made by RazenGear (Vincent Gunnink)
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Opdracht_9_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Asks the user how the test document should be named.
            Console.WriteLine("How should this text document be named?");
            string fileName = Console.ReadLine();
            string file = Environment.CurrentDirectory + "\\" + fileName + ".txt";

            //Checks if there is a file already named like the given name.
            if (File.Exists(file))
            {
                Console.Clear();
                Console.WriteLine($"Their is already a file named {fileName}, do you want to overwrite it? (y/n)");
                string userInput = Console.ReadLine();
                if (userInput.Contains("n"))
                {
                    Environment.Exit(1);
                }
            }
            //Asks the user to write something and then save the document.
            Console.Clear();
            Console.WriteLine("Type something and then press enter to save your text in the document.");
            using (StreamWriter text = File.CreateText(file))
            {
                text.WriteLine(Console.ReadLine());
            }
            System.Diagnostics.Process.Start(file);
            Environment.Exit(1);
        }
    }
}
