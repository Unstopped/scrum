using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;
using System.IO;

namespace Opdracht_6_15
{
    class Program
    {
        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }
            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        static bool IsWeekend()
        {
            //Get Local Time
            DateTime now = DateTime.Now;
            //DateTime now = new DateTime(2019, 12, 31);

            //Kijk of het weekend is of niet
            bool weekend;
            if ((now.ToString("dddd") == "Saturday") || (now.ToString("dddd") == "Sunday"))
            {
                weekend = true;
            }
            else
            {
                weekend = false;
            }
            //geef de output
            return weekend;
        }
        static bool Schrikkeljaar(int year)
        {

            bool schrikkeljaar;
            if (DateTime.IsLeapYear(year))
            {
                schrikkeljaar = true;
            }
            else
            {
                schrikkeljaar = false;
            }
            return (schrikkeljaar);
        }
        static void Main(string[] args)
        {
            //Kijk welke datum heyt nu is
            DateTime now = DateTime.Now;
            //DateTime now = new DateTime(2050, 12, 31);


            //Kijk welk jaar het is
            Console.WriteLine("Jaar: {0}", now.Year);

            //kijk welke maand het is
            Console.WriteLine("Maand: " + now.ToString("MMMM", new System.Globalization.CultureInfo("nl-NL")));

            //schrijf de dag in woorden
            Console.WriteLine("Dag (Woord): " + now.ToString("dddd", new System.Globalization.CultureInfo("nl-NL")));

            //schrijf de dag in nummers
            Console.WriteLine("Dag: " + now.Day);

            //schrijf het weeknummer
            Console.WriteLine("Week numer: " + GetIso8601WeekOfYear(now));

            //schrijf de dag in het jaar
            Console.WriteLine("Dag in het jaar: " + now.DayOfYear);

            Console.WriteLine(now);

            //kijk of het weekend is of niet
            if (IsWeekend() == true)
            {
                Console.WriteLine("Het is weekend!");
            }
            else
            {
                Console.WriteLine("Het is een werkdag!");
            }

            //kijk of het een Schrikkeljaar is
            if (Schrikkeljaar(now.Year) == true)
            {
                Console.WriteLine("het is een schrikkeljaar!");
            }
            else
            {
                Console.WriteLine("het is geen schrikkeljaar!");
            }

            Console.WriteLine("");

            //Opdracht 9.7
            //0 = voornaam
            //1 = achternaam
            //2 = geboortdatum
            //3 = studentenummer
            //4 = geboorteplaats
            // ... Create 2D array of strings.
            string[,] array = new string[,]
            {
            {"Syon", "Foppen", "10 maart 2002", "0308676", "Assen"},
            {"Erel", "Cetin", "2 augustus 2001", "0302077", "Enschede"},
            {"Job", "Van der Sluis", "24 maart 2002", "0311932", "Enschede"},
            {"Rico", "Boerdam", "30 april 2002", "0314452", "Brummen"},
            {"Vincent", "Gunnink", "24 juli 2001", "0308612", "Enschede"},
            {"Emanuel", "de Jong", "10 april 2000", "0307897", "Enschede"}
            };
            //neem de datum van vandaag
            

            //maak een list
            List<DateTime> geboortedatums = new List<DateTime>();


            //plaats de geboorte datums van het tekstbestand in een list
            foreach (string line in File.ReadLines(@"Resources\Verjaardag.txt", Encoding.UTF8))
            {
                geboortedatums.Add(DateTime.ParseExact(line, "dd MMMM yyyy", new System.Globalization.CultureInfo("nl-NL")));
            }
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;

            Console.WriteLine(String.Format("{0,-10}|{1,-10}|{2,-10}|{3,-10}|{4,-10}|{5,-10}|{6,-10}|", "      ", array[0, 0], array[1, 0], array[2, 0], array[3, 0], array[4, 0], array[5, 0]));
            Console.WriteLine("----------|----------|----------|----------|----------|----------|----------|");


            int a = 0;

            List<DateTime> datums = new List<DateTime>();
            while (a <= 10)
            {
                foreach (var data in geboortedatums)
                {
                    datums.Add(new DateTime(now.Year + a, data.Month, data.Day));
                }

                Console.WriteLine(String.Format("{0,-10}|{1,-10}|{2,-10}|{3,-10}|{4,-10}|{5,-10}|{6,-10}|", now.Year + a, datums[0].DayOfWeek, datums[1].DayOfWeek, datums[2].DayOfWeek, datums[3].DayOfWeek, datums[4].DayOfWeek, datums[5].DayOfWeek));
                Console.WriteLine("----------|----------|----------|----------|----------|----------|----------|");
                datums.Clear();
                a += 1;
            }
            
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;


        }
    }
}
