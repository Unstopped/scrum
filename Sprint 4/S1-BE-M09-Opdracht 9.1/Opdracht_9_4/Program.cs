using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_9_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("voer een karakter in die u wilt gebruiken om een laadscherm te maken?");
            Console.Write("karakter: ");
            string karakter = Console.ReadLine();
            Console.Write("Hoeveel karakters: ");
            int teller = Convert.ToInt32(Console.ReadLine());
            Console.Write("Voortgang: ");
            int j = 1;
            int i = 1;
            double pteller = (100 / teller);
            double percentage = 0;
            //een while loop die een bij I++ bij optelt en bij J-- afhaalt
            while (teller >= i)
            {
                Console.SetCursorPosition(11, 2);
                //de code voor de precentage
                if (i > 0)
                {
                    percentage = pteller * i;
                     
                    if(percentage < 100 && teller == i)
                    {
                        percentage = percentage + (100 - percentage);
                    }
                    Console.Write(Convert.ToString(Convert.ToInt32(percentage) + "% "));
                }
                else
                {
                    
                    Console.Write("0");
                }
                i++;
                //telkens een position eerder een teken plaatsen
                Console.SetCursorPosition(14 + teller + j--  , 2);
                Console.Write(karakter);
                //wachten voor 500ms
                System.Threading.Thread.Sleep(500);
            }

            Console.ReadKey();
        }
    }
}
