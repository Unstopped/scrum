using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_9_2
{
    class Program
    {
        static int locationCirkelX = 25;
        static int locationCirkelY = 1;

        public static void CreateCirkle()
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.SetCursorPosition(locationCirkelX, locationCirkelY);
            Console.Write("  *******  ");
            Console.SetCursorPosition(locationCirkelX, (locationCirkelY + 1));
            Console.Write(" ********* ");
            Console.SetCursorPosition(locationCirkelX, (locationCirkelY + 2));
            Console.Write(" ********* ");
            Console.SetCursorPosition(locationCirkelX, (locationCirkelY + 3));
            Console.Write(" ********* ");
            Console.SetCursorPosition(locationCirkelX, (locationCirkelY + 4));
            Console.Write("  *******  ");
        }

        public static void CreateRedCirkle()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.SetCursorPosition(locationCirkelX, locationCirkelY);
            Console.Write("  *******  ");
            Console.SetCursorPosition(locationCirkelX, (locationCirkelY + 1));
            Console.Write(" ********* ");
            Console.SetCursorPosition(locationCirkelX, (locationCirkelY + 2));
            Console.Write(" ********* ");
            Console.SetCursorPosition(locationCirkelX, (locationCirkelY + 3));
            Console.Write(" ********* ");
            Console.SetCursorPosition(locationCirkelX, (locationCirkelY + 4));
            Console.Write("  *******  ");
        }

        public static void CreateOrangeCirkle()
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.SetCursorPosition(locationCirkelX, locationCirkelY);
            Console.Write("  *******  ");
            Console.SetCursorPosition(locationCirkelX, (locationCirkelY + 1));
            Console.Write(" ********* ");
            Console.SetCursorPosition(locationCirkelX, (locationCirkelY + 2));
            Console.Write(" ********* ");
            Console.SetCursorPosition(locationCirkelX, (locationCirkelY + 3));
            Console.Write(" ********* ");
            Console.SetCursorPosition(locationCirkelX, (locationCirkelY + 4));
            Console.Write("  *******  ");
        }

        public static void CreateGreenCirkle()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.SetCursorPosition(locationCirkelX, locationCirkelY);
            Console.Write("  *******  ");
            Console.SetCursorPosition(locationCirkelX, (locationCirkelY + 1));
            Console.Write(" ********* ");
            Console.SetCursorPosition(locationCirkelX, (locationCirkelY + 2));
            Console.Write(" ********* ");
            Console.SetCursorPosition(locationCirkelX, (locationCirkelY + 3));
            Console.Write(" ********* ");
            Console.SetCursorPosition(locationCirkelX, (locationCirkelY + 4));
            Console.Write("  *******  ");
        }

        public static void CreatePole()
        {
            Console.ForegroundColor = ConsoleColor.DarkGray;

            for (int i = 0; i < 15; i++)
            {
                locationCirkelY = locationCirkelY - 1;
                Console.SetCursorPosition(locationCirkelX, locationCirkelY);
                Console.Write("   *****   ");
            }
        }

        public static void Main(string[] args)
        {
            //Asks if you want to use the dutch traffic light system or the german one and uses that system based on your answer.
            Console.Clear();
            Console.WriteLine("Welk stoplicht systeem wilt u gebruiken?: (NL/DE)");
            string stopLichtSyst = Console.ReadLine().ToLower();

            if (stopLichtSyst == "de")
            {
                for (int i = 0; i < 5; i++)
                {
                    //Start of the traffic lights.
                    //creates the upper cirkle for the traffic light.
                    Console.Clear();
                    locationCirkelY = 1;
                    CreateRedCirkle();

                    //creates the middle cirkle for the traffic light.
                    locationCirkelY = 7;
                    CreateCirkle();

                    //creates the bottem cirkle for the traffic light.
                    locationCirkelY = 13;
                    CreateCirkle();

                    //creates the pole for the traffic light using a loop.
                    locationCirkelY = 34;
                    CreatePole();

                    //lets the red light on for five seconds.
                    System.Threading.Thread.Sleep(5000);

                    //turns off the red light.
                    locationCirkelY = 1;
                    CreateCirkle();

                    //turns on the orange light.
                    locationCirkelY = 7;
                    CreateOrangeCirkle();

                    //lets the orange light on for five seconds.
                    System.Threading.Thread.Sleep(2000);

                    //turns off the orange light.
                    locationCirkelY = 7;
                    CreateCirkle();

                    //turns on the green light.
                    locationCirkelY = 13;
                    CreateGreenCirkle();

                    //lets the green light on for five seconds.
                    System.Threading.Thread.Sleep(5000);

                    //turns off the green light.
                    locationCirkelY = 13;
                    CreateCirkle();

                    //turns on the orange light.
                    locationCirkelY = 7;
                    CreateOrangeCirkle();

                    //lets the orange light on for two seconds.
                    System.Threading.Thread.Sleep(2000);

                    //After 5 loops of the lights turning on and off, it asks if you want to continue with the app or not.
                    if (i == 4)
                    {
                        Console.Clear();
                        Console.ResetColor();
                        Console.WriteLine("Klaar! Wilt u afsluiten? (ja/nee)");
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                        string exitOption = Console.ReadLine().ToLower();
                        Console.ResetColor();

                        if (exitOption == "ja")
                        {
                            Console.Clear();
                            Console.WriteLine("Aan het afsluiten...");
                        }
                        if (exitOption == "nee")
                        {
                            Console.Clear();
                            Main(args);
                        }
                    }
                }
            }

            if (stopLichtSyst == "nl")
            {
                for (int i = 0; i < 5; i++)
                {
                    //Start of the traffic lights.
                    //creates the upper cirkle for the traffic light.
                    Console.Clear();
                    locationCirkelY = 1;
                    CreateRedCirkle();

                    //creates the middle cirkle for the traffic light.
                    locationCirkelY = 7;
                    CreateCirkle();

                    //creates the bottem cirkle for the traffic light.
                    locationCirkelY = 13;
                    CreateCirkle();

                    //creates the pole for the traffic light using a loop.
                    locationCirkelY = 34;
                    CreatePole();

                    //lets the red light on for five seconds.
                    System.Threading.Thread.Sleep(5000);

                    //turns off the red light.
                    locationCirkelY = 1;
                    CreateCirkle();

                    //turns on the green light.
                    locationCirkelY = 13;
                    CreateGreenCirkle();

                    //lets the green light on for five seconds.
                    System.Threading.Thread.Sleep(5000);

                    //turns off the green light.
                    locationCirkelY = 13;
                    CreateCirkle();

                    //turns on the orange light.
                    locationCirkelY = 7;
                    CreateOrangeCirkle();

                    //lets the orange light on for two seconds.
                    System.Threading.Thread.Sleep(2000);

                    //After 5 loops of the lights turning on and off, it asks if you want to continue with the app or not.
                    if (i == 4)
                    {
                        Console.Clear();
                        Console.ResetColor();
                        Console.WriteLine("Klaar! Wilt u afsluiten? (ja/nee)");
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                        string exitOption = Console.ReadLine().ToLower();
                        Console.ResetColor();

                        if (exitOption == "ja")
                        {
                            Console.Clear();
                            Console.WriteLine("Aan het afsluiten...");
                        }
                        if (exitOption == "nee")
                        {
                            Console.Clear();
                            Main(args);
                        }
                    }
                }
            }

            else
            {
                Console.Clear();
                Console.WriteLine("Dat is geen keuze, probeer opnieuw.");
                System.Threading.Thread.Sleep(3000);
                Main(args);
            }
        }
    }
}