using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_5_19_FIXD
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Voer een tempratuur in:");
            int temp = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("\n\n+50");
            Console.BackgroundColor = ConsoleColor.White;
            Console.WriteLine(" ");
            Console.ResetColor();
            if (temp >= -30 && temp <= 50)
            {
                if (temp >= -30 && temp <= -20)
                {
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.BackgroundColor = ConsoleColor.Magenta;
                    Console.WriteLine(" ");
                }
                else if (temp >= -20 && temp <= -10)
                {
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.BackgroundColor = ConsoleColor.DarkGray;
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                }
                else if (temp >= -10 && temp <= 0)
                {                 
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.BackgroundColor = ConsoleColor.Cyan;
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                }
                else if (temp >= 0 && temp <= 10)
                {                   
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                }
                else if (temp >= 10 && temp <= 20)
                {
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.BackgroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                }
                else if (temp >= 20 && temp <= 30)
                {
                    Console.WriteLine(" ");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                }

                else if (temp >= 30 && temp <= 50)
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                }

            }
            else
            {
                Console.WriteLine("Vul een tempratuur in tussen de -30 en 50 C");
            }
            Console.BackgroundColor = ConsoleColor.White;
            Console.WriteLine(" ");
            Console.ResetColor();
            Console.WriteLine("-30");
            Console.ReadLine();
            Console.Clear();
            Main(args);
        }
    }
}

