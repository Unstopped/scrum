using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_3._5
{
    class Program
    {
        static void Main(string[] args)
        {
            
            //Asks for the width of the camper and stores the data in the variable "breedte".
            Console.WriteLine("Wat is de breedte van de camper in meters?");
            double breedte = Convert.ToDouble(Console.ReadLine());

            //Asks for the length of the camper and stores the data in the variable "lengte".
            Console.WriteLine("Wat is de lengte van de camper in meters?");
            double lengte = Convert.ToDouble(Console.ReadLine());

            //The price for every square meter per month is stored in the variable "PrijsPerMaand".
            double PrijsPerMaand = lengte * breedte * 1.50;

            //The discount for a contract of 2 years in the variable "KortingJaar2" and for a contract of 3 years, in %.
            int KortingJaar2 = 5;
            int KortingJaar3 = 10;

            //Calculates the price per year for each contract and sets the outcome of it, equal to the Variables "PrijsJaar1", "PrijsJaar2" and "PrijsJaar3".
            double PrijsJaar1 = PrijsPerMaand * 12;
            double PrijsJaar2 = ((PrijsPerMaand - (PrijsPerMaand / 100 * KortingJaar2)) * 12) * 2;
            double PrijsJaar3 = ((PrijsPerMaand - (PrijsPerMaand / 100 * KortingJaar3)) * 12) * 2;

            
            //Outputs the final price for every year.
            Console.WriteLine("De prijs voor een 1 jarig contract = " + PrijsJaar1);
            Console.WriteLine("De prijs voor een 2 jarig contract = " + PrijsJaar2);
            Console.WriteLine("De prijs voor een 3 jarig contract = " + PrijsJaar3);
        }
    }
}
