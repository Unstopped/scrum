using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Opdracht_3_7
{
    class Program
    {
        static void Main(string[] args)
        {
            //Vraag voor het woord.
            Console.Write("Voer een woord in: ");
            string woord = Console.ReadLine();

            //geef aan welke letters klinkers zijn
            char[] vowels = { 'a', 'e', 'i', 'o', 'u', 'y', 'A', 'E', 'I', 'O', 'U', 'Y' };

            //replace de klinkers met *
            foreach (char vowel in vowels)
                woord = woord.Replace(vowel, '*');

            //laat het woord zien
            Console.WriteLine(woord);

            Console.ReadKey();
        }
    }
}
