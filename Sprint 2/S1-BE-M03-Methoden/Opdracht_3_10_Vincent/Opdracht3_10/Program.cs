using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht3_10
{
    class Program
    {
        static void Main(string[] args)
        {
            var time = new System.Diagnostics.Stopwatch();                  //Creates a stopwatch named 'time'.

            Console.WriteLine("Hello, What is your name?");                 //Asks the user what his name is.
            string name = Console.ReadLine();                               //Stores the users anwser as 'name'.
            time.Start();                                                   //Starts the stopwatch 'time'.

            Console.WriteLine($"\nHow old are you {name} ?");               //Asks 'name' how old he/she is.
            string age = Console.ReadLine();                                //Stores the users anwser as 'age'.
            time.Stop();                                                    //Stops the stopwatch 'time'.

            TimeSpan timeSpend = time.Elapsed;                              //Stores the time elapsed by 'time' in 'timeSpend'.

            string timeSec = String.Format("{0:00}", timeSpend.Seconds);    //Formats 'timeSpend' to seconds, And stores the formated awnser in 'timeSec'.

            Console.WriteLine($"\n{name} ({age}) It took you {timeSec} seconds to answer the questions.");  //Tells the users 'name', 'age' and 'timeSec'.

            Console.ReadKey();
        }
    }
}
