using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_3_11
{
    class Program
    {
        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }
            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }
        static void Main(string[] args)
        {
            //write everything in the console
            DateTime now = DateTime.Now;
            Console.WriteLine("Jaar: {0}", now.Year);
            Console.WriteLine("Maand: " + now.ToString("MMMM", new System.Globalization.CultureInfo("nl-NL")));
            Console.WriteLine("Dag (Woord): " + now.ToString("dddd", new System.Globalization.CultureInfo("nl-NL")));
            Console.WriteLine("Dag: " + now.Day);
            Console.WriteLine("Week numer: " + GetIso8601WeekOfYear(now));
            Console.WriteLine("Dag in het jaar: " + now.DayOfYear);
            Console.ReadKey();
            
        }

        
    }
}
