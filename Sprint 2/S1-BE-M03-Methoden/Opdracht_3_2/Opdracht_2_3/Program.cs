using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_2_3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Get current time
            DateTime now = DateTime.Now;

            //Vraag naar de geboorte datum van de vrouw
            Console.Write("Geef de geboorte datum van de vrouw: ");
            string GD_Vrouw = Console.ReadLine();
            DateTime Date_Vrouw = DateTime.Parse(GD_Vrouw);

            //Kijk hoeveel dagen oud de vrouw is
            TimeSpan L_dagen_vrouw = now - Date_Vrouw;

            Console.Write("Geef de geboorte datum van de man: ");
            string GD_Man = Console.ReadLine();
            DateTime Date_Man = DateTime.Parse(GD_Man);

            //Kijk hoeveel dagen oud de man is
            TimeSpan L_dagen_Man = now - Date_Man;

            //kijk of de man of de vrouw ouder is om negatieve getallen te verkomen.
            if (L_dagen_Man.Days > L_dagen_vrouw.Days)
            {
                //Bereken het vershil in dagen
                int verschil = L_dagen_Man.Days - L_dagen_vrouw.Days;
                Console.WriteLine("\nDe man is {0} dagen ouder dan de vrouw.", verschil);
            }
            else if (L_dagen_vrouw.Days > L_dagen_Man.Days)
            {
                //Bereken het vershil in dagen
                int verschil = L_dagen_vrouw.Days - L_dagen_Man.Days;
                Console.WriteLine("\nDe vrouw is {0} dagen ouder dan de man.", verschil);
            }
            else if (L_dagen_vrouw.Days == L_dagen_Man.Days)
            {
                Console.WriteLine("\nDe vrouw en de man zijn even oud.");
            }

            Console.ReadKey();

        }
    }
}
