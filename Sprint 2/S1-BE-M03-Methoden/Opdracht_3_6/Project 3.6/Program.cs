using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_3._6
{
    class Program
    {
        static void Main(string[] args)
        {
            //Random
            Random rnd = new Random();

            //Creates a student ID.
            int studentID = rnd.Next(1000, 9999);

            //Dutch grades.
            //Asks for the first grade and stores the data in the variable nedFin1.
            Console.WriteLine("Wat is je eerste cijfer voor Nederlands?");
            double nedFin1 = Convert.ToDouble(Console.ReadLine());
            //Asks for the second grade and stores the data in the variable.
            Console.WriteLine("Wat is je tweede cijfer voor Nederlands?");
            double nedFin2 = Convert.ToDouble(Console.ReadLine());
            //Calculates the average grade.
            var nedGem = (nedFin1 + nedFin2) / 2;


            //English grades.
            //Asks for the first grade and stores the data in the variable engFin1.
            Console.WriteLine("Wat is je eerste cijfer voor Engels?");
            double engFin1 = Convert.ToDouble(Console.ReadLine());
            //Asks for the second grade and stores the data in the variable engFin2.
            Console.WriteLine("Wat is je tweede cijfer voor Engels?");
            double engFin2 = Convert.ToDouble(Console.ReadLine());
            //Calculates the average grade.
            double engGem = (engFin1 + engFin2) / 2;

            //Math grades.
            //Asks for the first grade and stores the data in the variable wisFin1.
            Console.WriteLine("Wat is je eerste cijfer voor Wiskunde?");
            double wisFin1 = Convert.ToDouble(Console.ReadLine());
            //Asks for the second grade and stores the data in the variable wisFin2.
            Console.WriteLine("Wat is je tweede cijfer voor Wiskunde?");
            double wisFin2 = Convert.ToDouble(Console.ReadLine());
            //Calculates the average grade.
            double wisGem = (wisFin1 + wisFin2) / 2;

            //PRG grades.
            //Asks for the first grade and stores the data in the variable prgFin1.
            Console.WriteLine("Wat is je eerste cijfer voor PRG?");
            double prgFin1 = Convert.ToDouble(Console.ReadLine());
            //Asks for the second grade and stores the data in the variable prgFin2.
            Console.WriteLine("Wat is je tweede cijfer voor PRG?");
            double prgFin2 = Convert.ToDouble(Console.ReadLine());
            //Calculates the average grade.
            double prgGem = (prgFin1 + (prgFin2 * 2)) / 3;

            //Database design grades.
            //Asks for the first grade and stores the data in the variable dbdFin1.
            Console.WriteLine("Wat is je eerste cijfer voor database design?");
            double dbdFin1 = Convert.ToDouble(Console.ReadLine());
            //Asks for the second grade and stores the data in the variable dbdFin2.
            Console.WriteLine("Wat is je tweede cijfer voor database design?");
            double dbdFin2 = Convert.ToDouble(Console.ReadLine());
            //Calculates the average grade.
            double dbdGem = (dbdFin1 + (dbdFin2 * 2)) / 3;

            //Algorithm grades.
            //Asks for the first grade and stores the data in the variable algFin1.
            Console.WriteLine("Wat is je eerste cijfer voor algoritmiek");
            double algFin1 = Convert.ToDouble(Console.ReadLine());
            //Asks for the second grade and stores the data in the variable algFin2.
            Console.WriteLine("Wat is je tweede cijfer voor algoritmiek?");
            double algFin2 = Convert.ToDouble(Console.ReadLine());
            //Calculates the average grade.
            double algGem = (algFin1 + (algFin2 * 2)) / 3;

            //Login screen. (makes you fill in your student ID)
            Console.WriteLine("Log in met je leerlingnummer: " + studentID + "\n");
            int login = Convert.ToInt32(Console.ReadLine());

            //If you type in the correct student ID then it shows your grade list.
            if (login == studentID)
            {
                Console.Clear();
                Console.WriteLine("\nAan het inloggen...");
                Console.WriteLine("\n");
                Console.WriteLine("|    Vak: NED    | " + nedFin1 + "   |  " + nedFin2 + "    |   Gemiddelde:  | " + nedGem + "    ");
                Console.WriteLine("|    Vak: ENG    | " + engFin1 + "   |  " + engFin2 + "    |   Gemiddelde:  | " + engGem + "    ");
                Console.WriteLine("|    Vak: WIS    | " + wisFin1 + "   |  " + wisFin2 + "    |   Gemiddelde:  | " + wisGem + "    ");
                Console.WriteLine("|    Vak: PRG    | " + prgFin1 + "   |  " + prgFin2 + "    |   Gemiddelde:  | " + prgGem + "    ");
                Console.WriteLine("|    Vak: DBD    | " + dbdFin1 + "   |  " + dbdFin2 + "    |   Gemiddelde:  | " + dbdGem + "    ");
                Console.WriteLine("|    Vak: ALG    | " + algFin1 + "   |  " + algFin2 + "    |   Gemiddelde:  | " + algGem + "    " + "\n");

            }
            //Else it will show you that you didnt type in the correct student ID and will end the application.
            else
            {
                Console.WriteLine("\nDat is niet je leerlingnummer! probeer het later opnieuw.\n");
            }
        }
    }
}
