using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace opdracht_3_8
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            Console.WriteLine("geef je voornaam:");
            string vnaam = Console.ReadLine();
            Console.WriteLine("geef je tussenvoegsel:");
            string tvoegsel = Console.ReadLine();
            Console.WriteLine("geef je achternaam:");
            string anaam = Console.ReadLine();

            int lengte = anaam.Length + vnaam.Length + tvoegsel.Length;
            string username = vnaam.Substring(0, 1) + anaam + lengte;
            Console.Write(username.ToLower());
            Console.ReadKey();
        }
    }
}
