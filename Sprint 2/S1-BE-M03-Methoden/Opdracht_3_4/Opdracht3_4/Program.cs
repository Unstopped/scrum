using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht3_4
{
    class Program
    {
        static void Main(string[] args)
        {
            double euro; //Makes a variable.
            Console.WriteLine("Type the amount of money you want to convert:"); //Displays a line with text.
            euro = Convert.ToDouble(Console.ReadLine()); //Gets a value from user and converts it to Double.

            Console.Clear();
            Console.WriteLine("USD : " + (euro * 1.1236));  //Displays a line with the awnser of a calculation as text.
            Console.WriteLine("CAD : " + (euro * 1.4872));  //  ^
            Console.WriteLine("GBP : " + (euro * 0.8710));  //  |
            Console.WriteLine("DKK : " + (euro * 7.4613));  //  |
            Console.WriteLine("RUB : " + (euro * 76.0756)); //  |
            Console.ReadKey();
        }
    }
}
