using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_5._9
{
    class Program
    {
        static void Main(string[] args)
        {
            //variables for later.
            long machtAnt;
            int startGetal = 0;

            //Asks to pick a number between 10 and 20 and stores whatever number you picked in the variable eindGetal.
            Console.WriteLine("Kies een getal tussen de 10 en 20");
            long eindGetal = Convert.ToInt64(Console.ReadLine());

            //If eindgetal is lower than ten or higher than twenty then it says its not a number between ten and twenty.
            if (eindGetal < 10 || eindGetal > 20)
            {
                Console.WriteLine("Dat is niet een getal tussen de 10 en 20!");
            }

            //else it will loop, showing the calculations as 1 loop ends.
            else
            {
                //the loop.
                while (startGetal < eindGetal)
                {
                    startGetal = startGetal + 1;
                    machtAnt = Convert.ToInt64(Math.Pow(startGetal, startGetal));
                    Console.WriteLine(startGetal + " | " + startGetal + " tot de macht " + startGetal + " = " + machtAnt);
                }
            }
        }
    }
}
