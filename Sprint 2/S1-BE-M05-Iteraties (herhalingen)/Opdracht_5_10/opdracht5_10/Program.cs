//Made by RazenGear (Vincent Gunnink)
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace opdracht5_10
{
    class Program
    {
        static void Main(string[] args)
        {
            //Defines two variables.
            int u;
            int total = 0;

            //Asks the usser for input and stores this in 'u'
            Console.WriteLine("Please enter a number between 10 and 100.");
            u = Convert.ToInt16(Console.ReadLine());

            //Checks and displays what numbers between 1 and the users input are odd numbers.
            for (int i = 1; i <= u; i++)
            {
                if (IsOdd(i))
                {
                    Console.WriteLine(i);
                    total = total + i;
                }
            }

            //Displays the total of all numbers combined
            Console.WriteLine("\n"+total);
            Console.ReadLine();

        //Defines a function to check if numbers are odd.
        }
        public static bool IsOdd(int value)
        {
            return value % 2 != 0;
        }
    }
}
