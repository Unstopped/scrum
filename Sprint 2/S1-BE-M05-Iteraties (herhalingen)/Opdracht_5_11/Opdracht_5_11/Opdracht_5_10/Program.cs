using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_5_10
{
    class Program
    {
        static void Main(string[] args)
        {
            // set de varibles
            double start_Kapitaal;
            double rente;
            double tijd = 10;
            double eindbedrag;

            //vraag voor het start bedrag
            Console.Write("Voer je start bedrag in: ");
            start_Kapitaal = Convert.ToDouble(Console.ReadLine());

            // vraag voor het rentepercentage
            Console.Write("Voer het Rentepercentage in: ");
            rente = Convert.ToDouble(Console.ReadLine()) / 100 + 1;

            //reken het eind bedrag uit
            eindbedrag = start_Kapitaal * Math.Pow(rente, tijd);

            //Laat het eind bedrag zien
            Console.WriteLine("\nHet eind bedrag is: " + Math.Round(eindbedrag, 2));
            

        }
    }
}
