﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_5_14
{
    class Program
    {
        //asks the user to input a decimal number and gives back its binary and hexadecimal value
        static void Main(string[] args)
        {
            //defines the title of the console
            Console.Title = "Opdracht_5_14";


            //prints out the programs function
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine(
                "***********************************************************************************\n" +
                "*  Dit programma geeft de binaire en hexadecimale waarde van uw ingegeven nummer  *\n" +
                "***********************************************************************************\n" +
                "\n");


            //assigns the variables
            string retry = string.Empty;
            string strNumber = string.Empty;
            string hexadecimal = string.Empty;
            string binary = string.Empty;
            string hexadecimalSymbol = string.Empty;
            int intNumber = 0;
            int remainder = 0;
            bool isValid = false;

            //do while loop for if the user wants to run the program again
            do
            {
                //asks the user for
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Geef een nummer in:");
                Console.ForegroundColor = ConsoleColor.DarkGray;
                strNumber = Console.ReadLine();


                //validates the input
                while (!isValid)
                {
                    if (!(int.TryParse(strNumber, out int result)))
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("\nHet moet een heel nummer zijn!");
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                        strNumber = Console.ReadLine();
                    }
                    else if (Convert.ToInt32(strNumber) > 100000)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("\nHet nummer moet onder de 100.000 zijn, zodat uw computer het kan verdragen!");
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                        strNumber = Console.ReadLine();
                    }
                    else
                    {
                        isValid = true;
                    }

                }
                Console.WriteLine("");


                //calculates
                intNumber = Convert.ToInt32(strNumber);
                while (intNumber > 0)
                {
                    remainder = intNumber % 2;
                    intNumber /= 2;
                    binary = remainder.ToString() + binary;
                }
                intNumber = Convert.ToInt32(strNumber);

                while (intNumber > 0)
                {
                    if ((intNumber % 16) < 10)
                        hexadecimal = intNumber % 16 + hexadecimal;
                    else
                    {
                        hexadecimalSymbol = "";

                        switch (intNumber % 16)
                        {
                            case 10: hexadecimalSymbol = "A"; break;
                            case 11: hexadecimalSymbol = "B"; break;
                            case 12: hexadecimalSymbol = "C"; break;
                            case 13: hexadecimalSymbol = "D"; break;
                            case 14: hexadecimalSymbol = "E"; break;
                            case 15: hexadecimalSymbol = "F"; break;
                        }

                        hexadecimal = hexadecimalSymbol + hexadecimal;
                    }

                    intNumber /= 16;
                }


                //prints out
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("De binaire waarde van {0} is:\n{1}\n", strNumber, binary);
                Console.WriteLine("De hexadecimale waarde van {0} is:\n{1}", strNumber, hexadecimal);


                //asks if the user wants to run the program again
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("\nWilt u het opnieuw proberen? (ja/nee)");
                Console.ForegroundColor = ConsoleColor.DarkGray;
                retry = Console.ReadLine();


                //tries if the users input contains "ja" (dutch for yes) or "nee" (dutch for no)
                while (!(retry.ToLower().Contains("ja") || retry.ToLower().Contains("nee")))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("\nOngeldig. Geef \'ja\' of \'nee\' in.");
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    retry = Console.ReadLine();
                }
                Console.Clear();
            } while (retry.ToLower().Contains("ja"));
        }
    }
}
