//Made by RazenGear (Vincent Gunnink)
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht5_8
{
    class Program
    {
        static void Main(string[] args)
        {
            //Defines the two main variables and one temporarily variable.
            int a = 0;
            int b = 1;
            int c;

            //Show the first 25 numbers in of Fibonacci formula.
            for (int i = 1; i <= 25; i++)
            {
                c = a;
                a = b;
                b = c + b;
                Console.WriteLine(a);
            }
            Console.ReadLine();
        }
    }
}
