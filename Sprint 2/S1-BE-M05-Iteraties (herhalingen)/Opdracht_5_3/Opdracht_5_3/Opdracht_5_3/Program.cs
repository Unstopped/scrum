using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_5_3
{
    class Program
    {
        static void Main(string[] args)
        {
            int Getal1 = 0;
            int Getal2 = 0;

            Console.Write("Voer het eerste getal in: ");
            Getal1 = Convert.ToInt32(Console.ReadLine());

            Console.Write("Voer het tweede getal in: ");
            Getal2 = Convert.ToInt32(Console.ReadLine());

            if (Getal1 < Getal2)
            { 
                for (int i = Getal1 + 1; i <= Getal2 - 1; i++)
                {
                    Console.WriteLine(i);
                }
            }
            else if (Getal1 > Getal2)
            {
                for (int i = Getal2 + 1; i <= Getal1 - 1; i++)
                {
                    Console.WriteLine(i);
                }
            }
            else
            {
                Console.WriteLine("\nDe getallen zijn hetzelfde!");
            }
            Console.ReadKey();
        }
    }
}
