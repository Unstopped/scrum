using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_5._16
{
    class Program
    {
        static void Main(string[] args)
        {
            //Asks for a sentence and converts it to all lower key letters and then puts it in an array.
            Console.WriteLine("Geef een zin in.");
            string s = Console.ReadLine().ToLower();
            char[] characters = s.ToCharArray();
                
            //for loop for every 2 letters in the array, make it a uppercase letter.
                for (int i = 1; i < characters.Length; i += 2)
                {
                    characters[i] = char.ToUpper(characters[i]);
                }

            //creates a new string with all the second letters as uppercase letter and then puts it out.
            s = new string(characters);
            Console.WriteLine(s);
        }
    }
}
