//Made by RazenGear (Vincent Gunnink)
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht5_6
{
    class Program
    {
        static void Main(string[] args)
        {
            int number;                 //Makes a int variable named 'number'.
            int total = 0;              //Makes a int variable named 'total' and sets it to 0.
            Random OTS = new Random();  //Makes a Random variable named 'OTS'.

            for (int diceT = 1; diceT <= 5; diceT++)    //Loops 5 times.
            {
                number = OTS.Next(1,7);     //Sets 'number' to a random value from 1 to 6.
                total = total + number;     //Sets 'total' to 'total' + 'number'.

                Console.WriteLine($"Dice {diceT}: {number}");   //Shows which dice threw which number. 
            }

            Console.WriteLine($"\nTotal: {total}.");            //Shows the total of all dices combined.
            Console.WriteLine("Average: " + total / 5 + ".");   //Shows the average of all dices combined. 

            Console.ReadLine();
        }
    }
}
