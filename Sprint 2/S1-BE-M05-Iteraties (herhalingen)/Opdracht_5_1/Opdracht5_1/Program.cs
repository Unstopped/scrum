//Made by RazenGear (Vincent Gunnink)
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht5_1
{
    class Program
    {
        static void Main(string[] args)
        {
            double getal;

            for (getal = 1; getal <= 25; getal++)
            {
                Console.WriteLine($"{getal}");
            }
			
			Console.ReadLine();
        }
    }
}
