//ALL CREDITS go to EMANUEL DE JONG who spend his WHOLE AFTERNOON to make this code as short and user friendly as possible
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tests
    {
        class Program
        {
            //prints out all the disarium numbers lower than a user defined number
            static void Main(string[] args)
            {
                //assigns the variables
                string number, strI, opnieuw;
                double calculation;

                //do while loop for if the user wants to run the program again
                do
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    //asks the user for string number
                    Console.WriteLine("Geef een getal tussen de 10 en 1.000.000 en dit programma zal u alle disarium nummers onder dit getal laten zien.\n");
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    number = Console.ReadLine();
                    Console.WriteLine("");

                    //tries if the input is between 10 and 1.000.000 and if it is an Integer
                    if (Int64.TryParse(number, out long result))
                    {
                        if (Convert.ToInt64(number) >= 10 && Convert.ToInt64(number) <= 1000000)
                        {
                            //calcules all the disarium numbers lower than string number
                                Console.ForegroundColor = ConsoleColor.DarkGreen;
                                for (Int64 I = 1; I <= Convert.ToInt64(number); I++)
                                {
                                    calculation = 0;
                                    strI = Convert.ToString(I);
                                    for (int i = 0; i < strI.Length; i++)
                                    {
                                        calculation += Math.Pow(Convert.ToDouble(strI[i].ToString()), Convert.ToDouble(i) + 1.0);
                                    }

                                    //Prints out all the desarium numbers below string number
                                    if (Convert.ToInt64(calculation) == I)
                                    {
                                        Console.WriteLine(I + " is een disarium nummer.");
                                    }
                                }
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Het nummer moet tussen 10 en 1.000.000 zijn!");
                        }
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Het moet een nummer zijn!");
                    }

                    //asks if the user wants to run the program again
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("\nWilt u het opnieuw proberen? (ja/nee)\n");
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    opnieuw = Console.ReadLine();

                    //tries if the users input contains "ja" (dutch for yes) or "nee" (dutch for no)
                    while (!(opnieuw.ToLower().Contains("ja") || opnieuw.ToLower().Contains("nee")))
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("\nOngeldig. Geef \'ja\' of \'nee\' in.\n");
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                        opnieuw = Console.ReadLine();
                    }
                    Console.Clear();
                } while (opnieuw.ToLower().Contains("ja"));
            }
        }
    }