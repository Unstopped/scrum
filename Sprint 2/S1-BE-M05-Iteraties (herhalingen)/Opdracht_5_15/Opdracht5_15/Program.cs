using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht5_15
{
    class Program
    {
        
        static void Main(string[] args)
        {
            string original= Console.ReadLine();
            //verwijder de spatie in de string
            string test = original.Replace(" ", String.Empty);
            //draai het om
            var reversed = new string(test.Reverse().ToArray());
            //kijk of het omgedraaid het zelfde is als de eerste input
            if (test == reversed)
            {
                Console.WriteLine(original + "  is een palindroom");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine(original + "  is geen palindroom");
                Console.ReadKey();
            }

        }
    }
}
                                                                                                                                                    