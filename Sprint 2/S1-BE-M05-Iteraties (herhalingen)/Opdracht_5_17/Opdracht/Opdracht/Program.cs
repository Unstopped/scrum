using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht
{
    class Program
    {
        static void Main(string[] args)
        {
            //zet de begin string
            string originalString = "";
            
            //Vraag voor het woord om om te draaien
            Console.Write("Voer een woord in: ");
            originalString = Console.ReadLine();

            //draai het woord om
            string reversedString = new string(originalString.Reverse().ToArray());

            //geef de output
            Console.WriteLine("\nHet resultaat is: " + reversedString);
        }
    }
}
