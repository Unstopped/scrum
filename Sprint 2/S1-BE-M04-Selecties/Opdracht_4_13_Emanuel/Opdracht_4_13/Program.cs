﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_4_13
{
    class Program
    {
        static void Main(string[] args)
        {
            //assigns variables
            int numer, numerKwadraat, laasteNumerKwadraat;

            //asks for input
            Console.WriteLine("Geef een getal in.");
            numer = Convert.ToInt16(Console.ReadLine());

            //calculates
            numerKwadraat = numer * numer;
            laasteNumerKwadraat = numerKwadraat % 10;
            if (laasteNumerKwadraat == numer){
                Console.WriteLine(numer+" is een automorf");
            }
            else
            {
                Console.WriteLine(numer + " is geen automorf");
            }

            //gives output
            Console.WriteLine("Een automorf is een getal waarvan het kwadraat eindigt op dezelfde cijfers als het getal zelf.");
            Console.ReadLine();
        }
    }
}
