using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht4_5
{
    class Program
    {
        static void Main(string[] args)
        {
            // Creates a variable.
            double points;

            // Asks the user to enter a ammount of points and stores the answer in 'points'.
            Console.WriteLine("Enter your points:");
            points = Convert.ToDouble(Console.ReadLine());

            // Determanes what the user's score is in American grades.
            if (points >= 8.5)
            {
                Console.WriteLine("You scored an A+ which is sufficient.");
            }
            else if (points >= 7.5 && points < 8.5)
            {
                Console.WriteLine("You scored an A which is sufficient.");
            }
            else if (points >= 7 && points < 7.5)
            {
                Console.WriteLine("You scored an B+ which is sufficient.");
            }
            else if (points >= 6.5 && points < 7)
            {
                Console.WriteLine("You scored an B which is sufficient.");
            }
            else if (points >= 6 && points < 6.5)
            {
                Console.WriteLine("You scored an C which is sufficient.");
            }
            else if (points >= 5.5 && points < 6)
            {
                Console.WriteLine("You scored an D which is insufficient.");
            }
            else if (points <= 5)
            {
                Console.WriteLine("You scored an F which is insufficient.");
            }

            Console.ReadKey();
        }
    }
}
