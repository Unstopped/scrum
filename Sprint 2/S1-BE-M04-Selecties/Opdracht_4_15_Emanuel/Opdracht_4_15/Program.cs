﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_4_15
{
    class Program
    {
        static void Main(string[] args)
        {
            //assigns variables
            Int32 wk, sk, nk, totaal;

            //asks for input
            Console.WriteLine("Wat is uw score voor wiskunde?");
            Int32.TryParse(Console.ReadLine(), out wk);
            Console.WriteLine("Wat is uw score voor scheikunde?");
            Int32.TryParse(Console.ReadLine(), out sk);
            Console.WriteLine("Wat is uw score voor natuurkunde?");
            Int32.TryParse(Console.ReadLine(), out nk);

            //calculates wether the user may participate in the exames or not
            totaal = wk + sk + nk;
            if((wk >= 60 && (sk >= 60 || nk >= 60) || totaal >= 180) && (!(wk < 40) && !(sk < 40) && !(nk < 40)))
            {
                Console.WriteLine("U bent toegelaten voor het exame!");
            }
            else{
                Console.WriteLine("U bent helaas niet toegelaten voor het exame.");
            }
            Console.ReadLine();
        }
    }
}
