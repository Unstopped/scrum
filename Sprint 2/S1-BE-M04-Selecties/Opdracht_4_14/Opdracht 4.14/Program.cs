using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_4._14
{
    class Program
    {
        static void Main(string[] args)
        {
            //Asks for your first number and stores the data in the variable firstNumber.
            Console.WriteLine("CALCULATOR v.1\n " + "\nVul je eerste getal in.");
            double firstNumber = Convert.ToInt32(Console.ReadLine());

            //Asks for your second number and stores the data in the variable secondNumber.
            Console.WriteLine("Vul je tweede getal in.");
            double secondNumber = Convert.ToInt32(Console.ReadLine());

            //Asks you to make a choice what you want to do.
            Console.WriteLine("Wat wil je doen? \n1 = optellen \n2 = aftrekken \n3 = delen \n4 = vermenigvuldigen");
            int choice = Convert.ToInt32(Console.ReadLine());

            //If choice equals 1 then displays the answer after a short calculation.
            if (choice == 1)
            {
                double answer = firstNumber + secondNumber;
                Console.WriteLine("\n Het antwoord is: {0}", answer);
            }
            //If choice equals 2 then displays the answer after a short calculation.
            if (choice == 2)
            {
                double answer = firstNumber - secondNumber;
                Console.WriteLine("\n Het antwoord is: {0}", answer);
            }
            //If choice equals 3 then displays the answer after a short calculation.
            if (choice == 3)
            {
                if (secondNumber == 0)
                {
                    Console.WriteLine("delen door nul is flauwekul");
                }
                
                else
                {
                    double answer = firstNumber / secondNumber;
                    Console.WriteLine("\n Het antwoord is: {0}", answer);
                }
            }
            //If choice equals 4 then displays the answer after a short calculation.
            if (choice == 4)
            {
                double answer = firstNumber * secondNumber;
                Console.WriteLine("\n Het antwoord is: {0}", answer);
            }
        }
    }
}
