using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a month number.");
            double mNumber = Convert.ToDouble(Console.ReadLine());

            //Chooses a month depandent on the user supplied number.
            if (mNumber == 1)
            {
                Console.WriteLine($"Month {mNumber} is January");
            }
            else if (mNumber == 2)
            {
                Console.WriteLine($"Month {mNumber} is February");
            }
            else if (mNumber == 3)
            {
                Console.WriteLine($"Month {mNumber} is March");
            }
            else if (mNumber == 4)
            {
                Console.WriteLine($"Month {mNumber} is April");
            }
            else if (mNumber == 5)
            {
                Console.WriteLine($"Month {mNumber} is May");
            }
            else if (mNumber == 6)
            {
                Console.WriteLine($"Month {mNumber} is June");
            }
            else if (mNumber == 7)
            {
                Console.WriteLine($"Month {mNumber} is July");
            }
            else if (mNumber == 8)
            {
                Console.WriteLine($"Month {mNumber} is August");
            }
            else if (mNumber == 9)
            {
                Console.WriteLine($"Month {mNumber} is September");
            }
            else if (mNumber == 10)
            {
                Console.WriteLine($"Month {mNumber} is October");
            }
            else if (mNumber == 11)
            {
                Console.WriteLine($"Month {mNumber} is November");
            }
            else if (mNumber == 12)
            {
                Console.WriteLine($"Month {mNumber} is December");
            }

            Console.ReadKey();
        }
    }
}
