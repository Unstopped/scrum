using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_4_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef Getal 1: ");
            int Getal1 = Convert.ToInt32(Console.ReadLine());

            Console.Write("Geef Getal 2: ");
            int Getal2 = Convert.ToInt32(Console.ReadLine());

            if (Getal1 < Getal2)
            {
                Console.WriteLine("Het tweede getal is het grootst!");

            }
            else if (Getal1 > Getal2)
            {
                Console.WriteLine("Het eerste getal is het grootst!");
            }
            else if (Getal1 == Getal2)
            {
                Console.WriteLine("\nDe getallen zijn gelijk aan elkaar!");
            }
            Console.ReadKey();
        }
    }
}
