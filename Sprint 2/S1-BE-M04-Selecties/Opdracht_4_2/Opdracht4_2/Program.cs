// Made by RazenGear (Vincent Gunnink)
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht4_2
{
    class Program
    {
        static void Main(string[] args)
        {
            double num1;    // Makes a variable named 'num1'.
            double num2;    // Makes a variable named 'num2'.
            double num3;    // Makes a variable named 'num3'.

            Console.WriteLine("Type three random numbers (press enter after each number).");    // Displays text.

            Console.WriteLine("Number 1");                  // Displays text.
            num1 = Convert.ToDouble(Console.ReadLine());    // Converts the user's input to Double, then sets 'num1' to this converted value.

            Console.WriteLine("Number 2");                  // Displays text.
            num2 = Convert.ToDouble(Console.ReadLine());    // Converts the user's input to Double, then sets 'num2' to this converted value.

            Console.WriteLine("Number 3");                  // Displays text.
            num3 = Convert.ToDouble(Console.ReadLine());    // Converts the user's input to Double, then sets 'num3' to this converted value.

            if (num3 < num1 && num3 < num2)      // Checks if 'num3' is smaller then 'num1' and 'num2'.
            {
                Console.WriteLine("Het derde getal is het kleinste van de drie.");      // Displays text.
            }
            else
            {
                Console.WriteLine("Het derde getal is niet het kleinste van de drie."); // Displays text.
            }
            Console.ReadKey();
        }
    }
}
