using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Opdracht_4_10
{
    class Program
    {
        

        static void Main(string[] args)
        {
            //Vraag voor het nummer van de dag
            Console.Write("Voer het nummer van de dag in: ");
            int dag = Convert.ToInt32(Console.ReadLine());

            if (dag == 7)
            {
                dag = 0;
            }


            //kijk welke dag bij het numer hoord
            Console.WriteLine(Enum.GetName(typeof(DayOfWeek), dag));
        }

    }
}
