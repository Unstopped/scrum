using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_3_11
{
    class Program
    {
        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }
            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }
        static void Main(string[] args)
        {
            //<Syon>
            DateTime now = new DateTime(2012,01,01);
            Console.WriteLine("Jaar: {0}", now.Year);
            //</syon>

            //<Vincent>
            Console.WriteLine("Maand: " + now.ToString("MMMM", new System.Globalization.CultureInfo("nl-NL")));
            //</Vincent>

            //<Rico>
            Console.WriteLine("Dag (Woord): " + now.ToString("dddd", new System.Globalization.CultureInfo("nl-NL")));
            //</Rico>

            //<Job>
            Console.WriteLine("Dag: " + now.Day);
            //</Job>

            //calculate the week number
            Console.WriteLine("Week numer: " + GetIso8601WeekOfYear(now));

            //<Emanuel>
            Console.WriteLine("Dag in het jaar: " + now.DayOfYear);
            //</Emanuel>

            //----------------------------------------------

            //<Job>
            int schrikkeljaar = now.Year;
            //</Job>

            //<erel>
            if (schrikkeljaar % 4 == 0 && schrikkeljaar % 100 == 0 && schrikkeljaar % 400 == 0)
            {
                Console.WriteLine("het jaar is geen schrikkeljaar");
            }
            else
            {
                Console.WriteLine("het jaar is een schrikkeljaar");
            }
            //</erel>

            //<vincent>
            DayOfWeek dag = now.DayOfWeek;
            //</vincent>

            //<Syon>
            int werkdag = Convert.ToInt32(dag);
            //</Syon>

            //<Emanuel>
            if (werkdag > 0 && werkdag < 5)
            {
                Console.WriteLine("het is een werkdag");
            }
            //</Emanuel>

            //<Rico>
            else
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("het is weekend");
            }
            Console.ReadKey();
            //</Rico>
        }
    }
}
