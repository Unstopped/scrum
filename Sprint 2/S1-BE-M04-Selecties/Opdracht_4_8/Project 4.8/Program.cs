using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_4._8
{
    class Program
    {
        static void Main(string[] args)
        {
            //Asks what language you want to use and remembers what choice you made by storing it in the variable langChoice.
            Console.WriteLine("Kies een taal:\n1 = Nederlands\n2 = Duits\n3 = Engels\n4 = Frans\n5 = Spaans\n6 = Pools");
            string langChoice = Console.ReadLine();

            //Stores the current month number in the variable currentMonth.
            int currentMonth = DateTime.Now.Month;

            //All names from months in different languages stored in arrays.
            string[] dutchMonths = {"Januari", "Februari", "Maart", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "December"};
            string[] germanMonths = { "Januar", "Februar", "Marz", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember" };
            string[] englishMonths = { "Januari", "Februari", "March", "April", "May", "June", "Juli", "August", "September", "Oktober", "November", "December" };
            string[] frenchMonths = { "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre" };
            string[] spanishMonths = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };
            string[] polishMonths = { "Styceń", "Luty", "Marzec", "Kwiecień", "Maj", "Cerwiec", "Lipiec", "Serpień", "Wrzesień", "Październik", "Listopad", "Grudzień" };

            //Selects the current month in another language  from the arrays.
            string dutchMonth = dutchMonths[currentMonth - 1];
            string germanMonth = germanMonths[currentMonth - 1];
            string englishMonth = englishMonths[currentMonth - 1];
            string frenchMonth = frenchMonths[currentMonth - 1];
            string spanishMonth = spanishMonths[currentMonth - 1];
            string polishMonth = polishMonths[currentMonth - 1];

            //If langChoice equals to 1 then write the current month in Dutch.
            if (langChoice == "1")
            {
                Console.Clear();
                Console.WriteLine("De huidige maand is: " + dutchMonth + "\n");
            }
            //If langChoice equals to 2 then write the current month in German.
            if (langChoice == "2")
            {
                Console.Clear();
                Console.WriteLine("Der aktuele monat ist: " + germanMonth + "\n");
            }
            //If langChoice equals to 3 then write the current month in English.
            if (langChoice == "3")
            {
                Console.Clear();
                Console.WriteLine("The current month is: " + englishMonth + "\n");
            }
            //If langChoice equals to 4 then write the current month in French.
            if (langChoice == "4")
            {
                Console.Clear();
                Console.WriteLine("Le mois en cours est: " + frenchMonth + "\n");
            }
            //If langChoice equals to 5 then write the current month in Spanish.
            if (langChoice == "5")
            {
                Console.Clear();
                Console.WriteLine("El mes actual es: " + spanishMonth + "\n");
            }
            //If langChoice equals to 1 then write the current month in Polish.
            if (langChoice == "6")
            {
                Console.Clear();
                Console.WriteLine("bieżący miesiąc to: " + polishMonth + "\n");
            }
        }
    }
}
