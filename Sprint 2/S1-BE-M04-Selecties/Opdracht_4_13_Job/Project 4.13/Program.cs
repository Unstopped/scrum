using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_4._13
{
    class Program
    {
        static void Main(string[] args)
        {
            //Asks for your number that you want to check
            Console.Write("Voer een getal in: ");
            //Converts the number from a string to a int.
            int getal = Convert.ToInt32(Console.ReadLine());

            //Sets the variable "macht" to the outcome of getal * getal.
            int macht = getal * getal;

            //makes a string with the getal variable and macht variable
            string getalString = getal + "";
            string machtString = macht + "";

            //checks if "Macht" ends with "Getal" if true then...
            if (machtString.EndsWith(getalString))
            {
                Console.WriteLine("\n" + getal + " is een automorf getal.");
            }

            //Else it desplays that it isnt an automorph number.
            else
            {
                Console.WriteLine("\n" + getal + " is geen automorf getal.");
            }
        }
    }
}
