using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_4_3
{
    class Program
    {
        static void Main(string[] args)
        {
            bool done = false;
            int n;
            string input;
            ConsoleKeyInfo key;
            while (!done)
            {
                do
                {
                    key = Console.ReadKey(true);
                    Console.WriteLine("voer een nummer in?");
                    input = Console.ReadLine();
                } while (int.TryParse(input, out n) == false);

                Console.Clear();
                int nummer = n;
                switch (nummer)
                {
                    case 1:
                        Console.WriteLine("klaveren");
                        break;
                    case 2:
                        Console.WriteLine("ruiten");
                        break;
                    case 3:
                        Console.WriteLine("harten");
                        break;
                    case 4:
                        Console.WriteLine("schoppen");
                        break;
                    default:
                        Console.WriteLine("je kan alleen maar nummers van 1 tm 4 invoeren");
                        Console.ReadKey();
                        break;
                }



                Console.WriteLine("Druk op enter om verder te gaan");
                Console.ReadKey();

                if (key.Key == ConsoleKey.Enter)
                {
                    done = true;
                }

            }



        }
    }
}

