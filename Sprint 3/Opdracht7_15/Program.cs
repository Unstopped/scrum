//Made by RazenGear (Vincent Gunnink)
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_7_15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the characters you don't want in the code: \n(If you don't want to block specific characters press space)");
            string ui = Console.ReadLine();

            List<string> na = new List<string>();

            for (int q = 0; q <= ui.Length - 1; q++)
            {
                na.Add(Convert.ToString(ui[q]));
            }

            Console.Clear();
            Console.WriteLine("Your code is:");
            Console.WriteLine(CodeMaker(na));
            Console.ReadKey();
        }
        public static string CodeMaker(List<string> na)
        {
            string defaultIndex = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            Random r = new Random();
            string c = "";
            int s;

            for (int i = 1; i <= 11; i++)
            {
                int x = 0;
                string p = "";

                while (x < na.Count)
                {
                    s = r.Next(1, defaultIndex.Length + 1);
                    p = Convert.ToString(defaultIndex[s]);

                    for (int z = 0; z <= na.Count - 1; z++)
                    {
                        if (!p.Contains(na[z]))
                        {
                            x = x + 1;
                        }
                        else
                        {
                            x = 0;
                        }
                    }
                }
                c = c + p;
            }
            return c;
        }
    }
}

