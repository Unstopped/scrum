//Made by RazenGear (Vincent Gunnink)
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_6_7
{
    class Program
    {
        static void Main(string[] args)
        {
            string retry; // Defines a string called retry.
            do
            {
                //Clears the screen and then explains what the programs does and waits for the users input.
                Console.Clear();
                Console.WriteLine("This program shows UserInput (ex: 10) Fibonacci numbers.\n");
                string inputString = Convert.ToString(Console.ReadLine());  //Stores the users input as a string.

                if (double.TryParse(inputString, out double result)) //Check if the users input is a number.
                {
                    double inputDouble = Convert.ToDouble(inputString); //Converts 'inputString' to a double.

                    //Shows the result of the calculation.
                    Console.WriteLine("\nResult:\n");
                    Console.WriteLine($"{Fibonacci(0, 1, inputDouble)}");
                }
                else
                {
                    //Tells the user to only type numbers.
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("\nPlease only enter numbers.");
                    Console.ForegroundColor = ConsoleColor.Gray;
                }
                //Asks if the user wants to try again.
                Console.WriteLine("\nWould you like to try again? (yes/no)\n");
                retry = Console.ReadLine(); //Saves the users anwser.

                //Asks the user if they want to run the program again and checks if their input was valid.
                while (!(retry.ToLower().Contains("yes") || retry.ToLower().Contains("no")))
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("\nInvalid!. answer \'yes\' or \'no\'.\n");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    retry = Console.ReadLine(); //Saves the users input.
                }
                Console.Clear();
            } while (retry.ToLower().Contains("yes")); //If the user entered 'yes' the program will restart
        }

        //Calculates the amount of Fibonacci numbers defined by the user.
        public static string Fibonacci(double one, double two, double inputDouble)
        {
            double temp;
            double fTemp;
            string sResult = "";

            for (int i = 0; i <= inputDouble; i++)
            {
                temp = one;
                one = two;
                two = temp + two;

                fTemp = two;

                sResult = sResult + Convert.ToString(fTemp) + " ";
            }
            
            return sResult;
        }
    }
}
