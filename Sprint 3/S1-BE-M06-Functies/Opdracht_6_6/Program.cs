//Made by RazenGear (Vincent Gunnink)
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht6_6
{
    class Program
    {
        static void Main(string[] args)
        {
            //Defines multiple values.
            string retry;
            int nCheckR = 0;
            string nCheck;

            do
            {
                //Explains to the user what the program does.
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("" +
                    "+--------------------------------------------------------------------------------------+\n" +
                    "|With this program you can calculate the power of the first number to the second number|\n" +
                    "+--------------------------------------------------------------------------------------+");

                //Asks the user to enter a number.
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("\nEnter the first number:");
                nCheck = Console.ReadLine();

                if (int.TryParse(nCheck, out nCheckR)) //Checks if the user entered a number.
                {
                    double first = Convert.ToDouble(nCheck);
                    
                    //Asks the user to enter a number.
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine("\nEnter the second number:");
                    nCheck = Console.ReadLine();
                    if (int.TryParse(nCheck, out nCheckR)) //Checks if the user entered a number.
                {
                        double second = Convert.ToDouble(nCheck);
                        
                        if (!(Machtsverheffen(first, second) >= 2147483646)) //Makes sure the user doesn't try to input big number.
                        {
                            //Shows the awnser.
                            Console.ForegroundColor = ConsoleColor.Gray;
                            Console.WriteLine($"\nResult:");
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine($"\n{first} ^ {second} = {Machtsverheffen(first, second)}");

                        }
                        else
                        {
                            //Warns to user that some stuff shut not be tried.
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine("\nPlease don't try to calculate the universe ;)");
                        }
                    }

                }
                else
                {
                    //Tells the user he didn't read the line that said "Please enter a number:".
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Please only enter numbers!");
                }

                //The restart and retry option.
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("\nWould you like to try again? (yes/no)\n");
                retry = Console.ReadLine();

                while (!(retry.ToLower().Contains("yes") || retry.ToLower().Contains("no")))
                {
                    Console.WriteLine("\nInvalid! enter \'yes\' or \'no\'\n");
                    retry = Console.ReadLine();
                }
                Console.WriteLine("\n\n");
            } while (retry.ToLower().Contains("yes"));
        }
        
        //Does a calculation.
        public static double Machtsverheffen(double first, double second)
        {
            double temp = first;
            double tempA = second - 1;

            for (double i = 1; i <= tempA; i++)
            {
                temp = temp * first;    
            }
            return temp;
        }
    }
}
