using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_6_15
{
    class Program
    {
        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }
            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        static bool IsWeekend()
        {
            //Get Local Time
            DateTime now = DateTime.Now;

            //Kijk of het weekend is of niet
            bool weekend;
            if ((now.ToString("dddd") == "Saturday") || (now.ToString("dddd") == "Sunday"))
            {
                weekend = true;
            }
            else
            {
                weekend = false;
            }
            //geef de output
            return weekend;
        }
        static bool Schrikkeljaar(int year)
        {

            bool schrikkeljaar;
            if (DateTime.IsLeapYear(year))
            {
                schrikkeljaar = true;
            }
            else
            {
                schrikkeljaar = false;
            }
            return (schrikkeljaar);
        }
        static void Main(string[] args)
        {
            //Kijk welke datum heyt nu is
            DateTime now = DateTime.Now;

            //Kijk welk jaar het is
            Console.WriteLine("Jaar: {0}", now.Year);

            //kijk welke maand het is
            Console.WriteLine("Maand: " + now.ToString("MMMM", new System.Globalization.CultureInfo("nl-NL")));

            //schrijf de dag in woorden
            Console.WriteLine("Dag (Woord): " + now.ToString("dddd", new System.Globalization.CultureInfo("nl-NL")));
                
            //schrijf de dag in nummers
            Console.WriteLine("Dag: " + now.Day);

            //schrijf het weeknummer
            Console.WriteLine("Week numer: " + GetIso8601WeekOfYear(now));
                
            //schrijf de dag in het jaar
            Console.WriteLine("Dag in het jaar: " + now.DayOfYear);
                
            //kijk of het weekend is of niet
            if (IsWeekend() == true)
            {
                Console.WriteLine("Het is weekend!");
            }
            else
            {
                Console.WriteLine("Het is een werkdag!");
            }

            //kijk of het een Schrikkeljaar is
            if (Schrikkeljaar(now.Year) == true)
            {
                Console.WriteLine("het is een schrikkeljaar!");
            }
            else
            {
                Console.WriteLine("het is geen schrikkeljaar!");
            }



        }
    }
}

            
