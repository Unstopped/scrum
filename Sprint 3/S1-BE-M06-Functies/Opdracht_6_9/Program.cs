using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_6_9
{
    class Program
    {
        static void Main(string[] args)
        {
            string seconds = DateTime.Now.ToString(format: "ss");
            string minutes = DateTime.Now.ToString(format: "mm");
            string hours = DateTime.Now.ToString(format: "HH");

            int second1 = Convert.ToInt32(seconds.Substring(1, 1));
            int second2 = Convert.ToInt32(seconds.Substring(0, 1));

            int minute1 = Convert.ToInt32(minutes.Substring(1, 1));
            int minute2 = Convert.ToInt32(minutes.Substring(0, 1));

            int hours1 = Convert.ToInt32(hours.Substring(1, 1));
            int hours2 = Convert.ToInt32(hours.Substring(0, 1));

            for (; ; )
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.SetCursorPosition(62, 25);
                Console.Write(" *");
                Console.SetCursorPosition(62, 26);
                Console.Write(" *");
                Console.SetCursorPosition(62, 30);
                Console.Write(" *");
                Console.SetCursorPosition(62, 31);
                Console.Write(" *");


                Console.SetCursorPosition(44, 25);
                Console.Write(" *");
                Console.SetCursorPosition(44, 26);
                Console.Write(" *");
                Console.SetCursorPosition(44, 30);
                Console.Write(" *");
                Console.SetCursorPosition(44, 31);
                Console.Write(" *");
                Console.ForegroundColor = ConsoleColor.DarkGray;

                //first second statement
                if (second1 == 1 || second1 == 2 || second1 == 3 || second1 == 4 || second1 == 5 || second1 == 6 || second1 == 7 || second1 == 8 || second1 == 9 || second1 == 10 || second1 == 0)
                {
                    if (second1 == 1)
                    {
                        Console.SetCursorPosition(75, 25);
                        Console.Write("    *");
                        Console.SetCursorPosition(75, 26);
                        Console.Write("   **");
                        Console.SetCursorPosition(75, 27);
                        Console.Write("  * *");
                        Console.SetCursorPosition(75, 28);
                        Console.Write(" *  *");
                        Console.SetCursorPosition(75, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(75, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(75, 31);
                        Console.Write("    *");
                    }

                    if (second1 == 2)
                    {
                        Console.SetCursorPosition(75, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(75, 26);
                        Console.Write("    *");
                        Console.SetCursorPosition(75, 27);
                        Console.Write("    *");
                        Console.SetCursorPosition(75, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(75, 29);
                        Console.Write("*    ");
                        Console.SetCursorPosition(75, 30);
                        Console.Write("*    ");
                        Console.SetCursorPosition(75, 31);
                        Console.Write("*****");
                    }
                    if (second1 == 3)
                    {
                        Console.SetCursorPosition(75, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(75, 26);
                        Console.Write("    *");
                        Console.SetCursorPosition(75, 27);
                        Console.Write("    *");
                        Console.SetCursorPosition(75, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(75, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(75, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(75, 31);
                        Console.Write("*****");
                    }

                    if (second1 == 4)
                    {
                        Console.SetCursorPosition(75, 25);
                        Console.Write("*   *");
                        Console.SetCursorPosition(75, 26);
                        Console.Write("*   *");
                        Console.SetCursorPosition(75, 27);
                        Console.Write("*   *");
                        Console.SetCursorPosition(75, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(75, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(75, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(75, 31);
                        Console.Write("    *");
                    }
                    if (second1 == 5)
                    {
                        Console.SetCursorPosition(75, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(75, 26);
                        Console.Write("*    ");
                        Console.SetCursorPosition(75, 27);
                        Console.Write("*    ");
                        Console.SetCursorPosition(75, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(75, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(75, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(75, 31);
                        Console.Write("*****");
                    }

                    if (second1 == 6)
                    {
                        Console.SetCursorPosition(75, 25);
                        Console.Write("*    ");
                        Console.SetCursorPosition(75, 26);
                        Console.Write("*    ");
                        Console.SetCursorPosition(75, 27);
                        Console.Write("*    ");
                        Console.SetCursorPosition(75, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(75, 29);
                        Console.Write("*   *");
                        Console.SetCursorPosition(75, 30);
                        Console.Write("*   *");
                        Console.SetCursorPosition(75, 31);
                        Console.Write("*****");
                    }
                    if (second1 == 7)
                    {
                        Console.SetCursorPosition(75, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(75, 26);
                        Console.Write("    *");
                        Console.SetCursorPosition(75, 27);
                        Console.Write("    *");
                        Console.SetCursorPosition(75, 28);
                        Console.Write("    *");
                        Console.SetCursorPosition(75, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(75, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(75, 31);
                        Console.Write("    *");
                    }

                    if (second1 == 8)
                    {
                        Console.SetCursorPosition(75, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(75, 26);
                        Console.Write("*   *");
                        Console.SetCursorPosition(75, 27);
                        Console.Write("*   *");
                        Console.SetCursorPosition(75, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(75, 29);
                        Console.Write("*   *");
                        Console.SetCursorPosition(75, 30);
                        Console.Write("*   *");
                        Console.SetCursorPosition(75, 31);
                        Console.Write("*****");
                    }

                    if (second1 == 9)
                    {
                        Console.SetCursorPosition(75, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(75, 26);
                        Console.Write("*   *");
                        Console.SetCursorPosition(75, 27);
                        Console.Write("*   *");
                        Console.SetCursorPosition(75, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(75, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(75, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(75, 31);
                        Console.Write("    *");
                    }

                    if (second1 == 10)
                    {
                        Console.SetCursorPosition(75, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(75, 26);
                        Console.Write("*   *");
                        Console.SetCursorPosition(75, 27);
                        Console.Write("*   *");
                        Console.SetCursorPosition(75, 28);
                        Console.Write("*   *");
                        Console.SetCursorPosition(75, 29);
                        Console.Write("*   *");
                        Console.SetCursorPosition(75, 30);
                        Console.Write("*   *");
                        Console.SetCursorPosition(75, 31);
                        Console.Write("*****");
                        second1 = 0;
                        second2 = second2 + 1;
                    }

                    if (Convert.ToString(second1) == "" || second1 == 0)
                    {
                        Console.SetCursorPosition(75, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(75, 26);
                        Console.Write("*   *");
                        Console.SetCursorPosition(75, 27);
                        Console.Write("*   *");
                        Console.SetCursorPosition(75, 28);
                        Console.Write("*   *");
                        Console.SetCursorPosition(75, 29);
                        Console.Write("*   *");
                        Console.SetCursorPosition(75, 30);
                        Console.Write("*   *");
                        Console.SetCursorPosition(75, 31);
                        Console.Write("*****");
                    }
                }
                //second second if statements
                if (second2 == 1 || second2 == 2 || second2 == 3 || second2 == 4 || second2 == 5 || second2 == 6 || second2 == 0)
                {
                    if (second2 == 1)
                    {
                        Console.SetCursorPosition(66, 25);
                        Console.Write("    *");
                        Console.SetCursorPosition(66, 26);
                        Console.Write("   **");
                        Console.SetCursorPosition(66, 27);
                        Console.Write("  * *");
                        Console.SetCursorPosition(66, 28);
                        Console.Write(" *  *");
                        Console.SetCursorPosition(66, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(66, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(66, 31);
                        Console.Write("    *");
                    }

                    if (second2 == 2)
                    {
                        Console.SetCursorPosition(66, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(66, 26);
                        Console.Write("    *");
                        Console.SetCursorPosition(66, 27);
                        Console.Write("    *");
                        Console.SetCursorPosition(66, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(66, 29);
                        Console.Write("*    ");
                        Console.SetCursorPosition(66, 30);
                        Console.Write("*    ");
                        Console.SetCursorPosition(66, 31);
                        Console.Write("*****");
                    }

                    if (second2 == 3)
                    {
                        Console.SetCursorPosition(66, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(66, 26);
                        Console.Write("    *");
                        Console.SetCursorPosition(66, 27);
                        Console.Write("    *");
                        Console.SetCursorPosition(66, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(66, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(66, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(66, 31);
                        Console.Write("*****");
                    }

                    if (second2 == 4)
                    {
                        Console.SetCursorPosition(66, 25);
                        Console.Write("*   *");
                        Console.SetCursorPosition(66, 26);
                        Console.Write("*   *");
                        Console.SetCursorPosition(66, 27);
                        Console.Write("*   *");
                        Console.SetCursorPosition(66, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(66, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(66, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(66, 31);
                        Console.Write("    *");
                    }

                    if (second2 == 5)
                    {
                        Console.SetCursorPosition(66, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(66, 26);
                        Console.Write("*    ");
                        Console.SetCursorPosition(66, 27);
                        Console.Write("*    ");
                        Console.SetCursorPosition(66, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(66, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(66, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(66, 31);
                        Console.Write("*****");
                    }

                    if (second2 == 6)
                    {
                        Console.SetCursorPosition(66, 25);
                        Console.Write("*    ");
                        Console.SetCursorPosition(66, 26);
                        Console.Write("*    ");
                        Console.SetCursorPosition(66, 27);
                        Console.Write("*    ");
                        Console.SetCursorPosition(66, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(66, 29);
                        Console.Write("*   *");
                        Console.SetCursorPosition(66, 30);
                        Console.Write("*   *");
                        Console.SetCursorPosition(66, 31);
                        Console.Write("*****");
                        second2 = 0;
                        minute1 = minute1 + 1;
                    }

                    if (Convert.ToString(second2) == "" || second2 == 0)
                    {
                        Console.SetCursorPosition(66, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(66, 26);
                        Console.Write("*   *");
                        Console.SetCursorPosition(66, 27);
                        Console.Write("*   *");
                        Console.SetCursorPosition(66, 28);
                        Console.Write("*   *");
                        Console.SetCursorPosition(66, 29);
                        Console.Write("*   *");
                        Console.SetCursorPosition(66, 30);
                        Console.Write("*   *");
                        Console.SetCursorPosition(66, 31);
                        Console.Write("*****");
                    }

                }
                //first minute if statements
                if (minute1 == 1 || minute1 == 2 || minute1 == 3 || minute1 == 4 || minute1 == 5 || minute1 == 6 || minute1 == 7 || minute1 == 8 || minute1 == 9 || minute1 == 10 || minute1 == 0)
                {
                    if (minute1 == 1)
                    {
                        Console.SetCursorPosition(57, 25);
                        Console.Write("    *");
                        Console.SetCursorPosition(57, 26);
                        Console.Write("   **");
                        Console.SetCursorPosition(57, 27);
                        Console.Write("  * *");
                        Console.SetCursorPosition(57, 28);
                        Console.Write(" *  *");
                        Console.SetCursorPosition(57, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(57, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(57, 31);
                        Console.Write("    *");
                    }

                    if (minute1 == 2)
                    {
                        Console.SetCursorPosition(57, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(57, 26);
                        Console.Write("    *");
                        Console.SetCursorPosition(57, 27);
                        Console.Write("    *");
                        Console.SetCursorPosition(57, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(57, 29);
                        Console.Write("*    ");
                        Console.SetCursorPosition(57, 30);
                        Console.Write("*    ");
                        Console.SetCursorPosition(57, 31);
                        Console.Write("*****");
                    }

                    if (minute1 == 3)
                    {
                        Console.SetCursorPosition(57, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(57, 26);
                        Console.Write("    *");
                        Console.SetCursorPosition(57, 27);
                        Console.Write("    *");
                        Console.SetCursorPosition(57, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(57, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(57, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(57, 31);
                        Console.Write("*****");
                    }

                    if (minute1 == 4)
                    {
                        Console.SetCursorPosition(57, 25);
                        Console.Write("*   *");
                        Console.SetCursorPosition(57, 26);
                        Console.Write("*   *");
                        Console.SetCursorPosition(57, 27);
                        Console.Write("*   *");
                        Console.SetCursorPosition(57, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(57, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(57, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(57, 31);
                        Console.Write("    *");
                    }

                    if (minute1 == 5)
                    {
                        Console.SetCursorPosition(57, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(57, 26);
                        Console.Write("*    ");
                        Console.SetCursorPosition(57, 27);
                        Console.Write("*    ");
                        Console.SetCursorPosition(57, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(57, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(57, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(57, 31);
                        Console.Write("*****");
                    }

                    if (minute1 == 6)
                    {
                        Console.SetCursorPosition(57, 25);
                        Console.Write("*    ");
                        Console.SetCursorPosition(57, 26);
                        Console.Write("*    ");
                        Console.SetCursorPosition(57, 27);
                        Console.Write("*    ");
                        Console.SetCursorPosition(57, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(57, 29);
                        Console.Write("*   *");
                        Console.SetCursorPosition(57, 30);
                        Console.Write("*   *");
                        Console.SetCursorPosition(57, 31);
                        Console.Write("*****");
                    }

                    if (minute1 == 7)
                    {
                        Console.SetCursorPosition(57, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(57, 26);
                        Console.Write("    *");
                        Console.SetCursorPosition(57, 27);
                        Console.Write("    *");
                        Console.SetCursorPosition(57, 28);
                        Console.Write("    *");
                        Console.SetCursorPosition(57, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(57, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(57, 31);
                        Console.Write("    *");
                    }

                    if (minute1 == 8)
                    {
                        Console.SetCursorPosition(57, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(57, 26);
                        Console.Write("*   *");
                        Console.SetCursorPosition(57, 27);
                        Console.Write("*   *");
                        Console.SetCursorPosition(57, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(57, 29);
                        Console.Write("*   *");
                        Console.SetCursorPosition(57, 30);
                        Console.Write("*   *");
                        Console.SetCursorPosition(57, 31);
                        Console.Write("*****");
                    }

                    if (minute1 == 9)
                    {
                        Console.SetCursorPosition(57, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(57, 26);
                        Console.Write("*   *");
                        Console.SetCursorPosition(57, 27);
                        Console.Write("*   *");
                        Console.SetCursorPosition(57, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(57, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(57, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(57, 31);
                        Console.Write("    *");
                    }

                    if (minute1 == 10)
                    {
                        Console.SetCursorPosition(57, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(57, 26);
                        Console.Write("*   *");
                        Console.SetCursorPosition(57, 27);
                        Console.Write("*   *");
                        Console.SetCursorPosition(57, 28);
                        Console.Write("*   *");
                        Console.SetCursorPosition(57, 29);
                        Console.Write("*   *");
                        Console.SetCursorPosition(57, 30);
                        Console.Write("*   *");
                        Console.SetCursorPosition(57, 31);
                        Console.Write("*****");
                        minute1 = 0;
                        minute2 = minute2 + 1;
                    }

                    if (Convert.ToString(minute1) == "" || minute1 == 0)
                    {
                        Console.SetCursorPosition(57, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(57, 26);
                        Console.Write("*   *");
                        Console.SetCursorPosition(57, 27);
                        Console.Write("*   *");
                        Console.SetCursorPosition(57, 28);
                        Console.Write("*   *");
                        Console.SetCursorPosition(57, 29);
                        Console.Write("*   *");
                        Console.SetCursorPosition(57, 30);
                        Console.Write("*   *");
                        Console.SetCursorPosition(57, 31);
                        Console.Write("*****");
                    }
                }
                //second minute if statements
                if (minute2 == 1 || minute2 == 2 || minute2 == 3 || minute2 == 4 || minute2 == 5 || minute2 == 6 || minute2 == 0)
                {
                    if (minute2 == 1)
                    {
                        Console.SetCursorPosition(48, 25);
                        Console.Write("    *");
                        Console.SetCursorPosition(48, 26);
                        Console.Write("   **");
                        Console.SetCursorPosition(48, 27);
                        Console.Write("  * *");
                        Console.SetCursorPosition(48, 28);
                        Console.Write(" *  *");
                        Console.SetCursorPosition(48, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(48, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(48, 31);
                        Console.Write("    *");
                    }

                    if (minute2 == 2)
                    {
                        Console.SetCursorPosition(48, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(48, 26);
                        Console.Write("    *");
                        Console.SetCursorPosition(48, 27);
                        Console.Write("    *");
                        Console.SetCursorPosition(48, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(48, 29);
                        Console.Write("*    ");
                        Console.SetCursorPosition(48, 30);
                        Console.Write("*    ");
                        Console.SetCursorPosition(48, 31);
                        Console.Write("*****");
                    }

                    if (minute2 == 3)
                    {
                        Console.SetCursorPosition(48, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(48, 26);
                        Console.Write("    *");
                        Console.SetCursorPosition(48, 27);
                        Console.Write("    *");
                        Console.SetCursorPosition(48, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(48, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(48, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(48, 31);
                        Console.Write("*****");
                    }

                    if (minute2 == 4)
                    {
                        Console.SetCursorPosition(48, 25);
                        Console.Write("*   *");
                        Console.SetCursorPosition(48, 26);
                        Console.Write("*   *");
                        Console.SetCursorPosition(48, 27);
                        Console.Write("*   *");
                        Console.SetCursorPosition(48, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(48, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(48, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(48, 31);
                        Console.Write("    *");
                    }

                    if (minute2 == 5)
                    {
                        Console.SetCursorPosition(48, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(48, 26);
                        Console.Write("*    ");
                        Console.SetCursorPosition(48, 27);
                        Console.Write("*    ");
                        Console.SetCursorPosition(48, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(48, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(48, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(48, 31);
                        Console.Write("*****");
                    }

                    if (minute2 == 6)
                    {
                        Console.SetCursorPosition(48, 25);
                        Console.Write("*    ");
                        Console.SetCursorPosition(48, 26);
                        Console.Write("*    ");
                        Console.SetCursorPosition(48, 27);
                        Console.Write("*    ");
                        Console.SetCursorPosition(48, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(48, 29);
                        Console.Write("*   *");
                        Console.SetCursorPosition(48, 30);
                        Console.Write("*   *");
                        Console.SetCursorPosition(48, 31);
                        Console.Write("*****");
                        minute2 = 0;
                        hours1 = hours1 + 1;
                        Console.Beep();
                    }

                    if (Convert.ToString(minute2) == "" || minute2 == 0)
                    {
                        Console.SetCursorPosition(48, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(48, 26);
                        Console.Write("*   *");
                        Console.SetCursorPosition(48, 27);
                        Console.Write("*   *");
                        Console.SetCursorPosition(48, 28);
                        Console.Write("*   *");
                        Console.SetCursorPosition(48, 29);
                        Console.Write("*   *");
                        Console.SetCursorPosition(48, 30);
                        Console.Write("*   *");
                        Console.SetCursorPosition(48, 31);
                        Console.Write("*****");
                    }
                }

                if (hours1 == 1 || hours1 == 2 || hours1 == 3 || hours1 == 4 || hours1 == 5 || hours1 == 6 || hours1 == 7 || hours1 == 8 || hours1 == 9 || hours1 == 10 || hours1 == 0)
                {
                    if (hours1 == 1)
                    {
                        Console.SetCursorPosition(39, 25);
                        Console.Write("    *");
                        Console.SetCursorPosition(39, 26);
                        Console.Write("   **");
                        Console.SetCursorPosition(39, 27);
                        Console.Write("  * *");
                        Console.SetCursorPosition(39, 28);
                        Console.Write(" *  *");
                        Console.SetCursorPosition(39, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(39, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(39, 31);
                        Console.Write("    *");
                    }

                    if (hours1 == 2)
                    {
                        Console.SetCursorPosition(39, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(39, 26);
                        Console.Write("    *");
                        Console.SetCursorPosition(39, 27);
                        Console.Write("    *");
                        Console.SetCursorPosition(39, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(39, 29);
                        Console.Write("*    ");
                        Console.SetCursorPosition(39, 30);
                        Console.Write("*    ");
                        Console.SetCursorPosition(39, 31);
                        Console.Write("*****");
                    }

                    if (hours1 == 3)
                    {
                        Console.SetCursorPosition(39, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(39, 26);
                        Console.Write("    *");
                        Console.SetCursorPosition(39, 27);
                        Console.Write("    *");
                        Console.SetCursorPosition(39, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(39, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(39, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(39, 31);
                        Console.Write("*****");
                    }

                    if (hours1 == 4)
                    {
                        Console.SetCursorPosition(39, 25);
                        Console.Write("*   *");
                        Console.SetCursorPosition(39, 26);
                        Console.Write("*   *");
                        Console.SetCursorPosition(39, 27);
                        Console.Write("*   *");
                        Console.SetCursorPosition(39, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(39, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(39, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(39, 31);
                        Console.Write("    *");
                    }

                    if (hours1 == 5)
                    {
                        Console.SetCursorPosition(39, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(39, 26);
                        Console.Write("*    ");
                        Console.SetCursorPosition(39, 27);
                        Console.Write("*    ");
                        Console.SetCursorPosition(39, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(39, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(39, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(39, 31);
                        Console.Write("*****");
                    }

                    if (hours1 == 6)
                    {
                        Console.SetCursorPosition(39, 25);
                        Console.Write("*    ");
                        Console.SetCursorPosition(39, 26);
                        Console.Write("*    ");
                        Console.SetCursorPosition(39, 27);
                        Console.Write("*    ");
                        Console.SetCursorPosition(39, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(39, 29);
                        Console.Write("*   *");
                        Console.SetCursorPosition(39, 30);
                        Console.Write("*   *");
                        Console.SetCursorPosition(39, 31);
                        Console.Write("*****");
                    }
                    if (hours1 == 7)
                    {
                        Console.SetCursorPosition(39, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(39, 26);
                        Console.Write("    *");
                        Console.SetCursorPosition(39, 27);
                        Console.Write("    *");
                        Console.SetCursorPosition(39, 28);
                        Console.Write("    *");
                        Console.SetCursorPosition(39, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(39, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(39, 31);
                        Console.Write("    *");
                    }

                    if (hours1 == 8)
                    {
                        Console.SetCursorPosition(39, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(39, 26);
                        Console.Write("*   *");
                        Console.SetCursorPosition(39, 27);
                        Console.Write("*   *");
                        Console.SetCursorPosition(39, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(39, 29);
                        Console.Write("*   *");
                        Console.SetCursorPosition(39, 30);
                        Console.Write("*   *");
                        Console.SetCursorPosition(39, 31);
                        Console.Write("*****");
                    }

                    if (hours1 == 9)
                    {
                        Console.SetCursorPosition(39, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(39, 26);
                        Console.Write("*   *");
                        Console.SetCursorPosition(39, 27);
                        Console.Write("*   *");
                        Console.SetCursorPosition(39, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(39, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(39, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(39, 31);
                        Console.Write("    *");
                    }

                    if (hours1 == 10)
                    {
                        Console.SetCursorPosition(39, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(39, 26);
                        Console.Write("*   *");
                        Console.SetCursorPosition(39, 27);
                        Console.Write("*   *");
                        Console.SetCursorPosition(39, 28);
                        Console.Write("*   *");
                        Console.SetCursorPosition(39, 29);
                        Console.Write("*   *");
                        Console.SetCursorPosition(39, 30);
                        Console.Write("*   *");
                        Console.SetCursorPosition(39, 31);
                        Console.Write("*****");
                        hours1 = 0;
                        hours2 = hours2 + 1;
                    }

                    if (Convert.ToString(hours1) == "" || hours1 == 0)
                    {
                        Console.SetCursorPosition(39, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(39, 26);
                        Console.Write("*   *");
                        Console.SetCursorPosition(39, 27);
                        Console.Write("*   *");
                        Console.SetCursorPosition(39, 28);
                        Console.Write("*   *");
                        Console.SetCursorPosition(39, 29);
                        Console.Write("*   *");
                        Console.SetCursorPosition(39, 30);
                        Console.Write("*   *");
                        Console.SetCursorPosition(39, 31);
                        Console.Write("*****");
                    }
                }

                if (hours2 == 1 || hours2 == 2 || hours2 == 3 || hours2 == 4 || hours2 == 5 || hours2 == 6)
                {
                    if (hours2 == 1)
                    {
                        Console.SetCursorPosition(30, 25);
                        Console.Write("    *");
                        Console.SetCursorPosition(30, 26);
                        Console.Write("   **");
                        Console.SetCursorPosition(30, 27);
                        Console.Write("  * *");
                        Console.SetCursorPosition(30, 28);
                        Console.Write(" *  *");
                        Console.SetCursorPosition(30, 29);
                        Console.Write("    *");
                        Console.SetCursorPosition(30, 30);
                        Console.Write("    *");
                        Console.SetCursorPosition(30, 31);
                        Console.Write("    *");
                    }

                    if (hours2 == 2)
                    {
                        Console.SetCursorPosition(30, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(30, 26);
                        Console.Write("    *");
                        Console.SetCursorPosition(30, 27);
                        Console.Write("    *");
                        Console.SetCursorPosition(30, 28);
                        Console.Write("*****");
                        Console.SetCursorPosition(30, 29);
                        Console.Write("*    ");
                        Console.SetCursorPosition(30, 30);
                        Console.Write("*    ");
                        Console.SetCursorPosition(30, 31);
                        Console.Write("*****");
                    }

                    if (hours2 == 2 && hours1 == 4)
                    {
                        second1 = 0;
                        second2 = 0;
                        minute1 = 0;
                        minute2 = 0;
                        hours1 = 0;
                        hours2 = 0;
                    }

                    if (Convert.ToString(hours2) == "" || hours2 == 0)
                    {
                        Console.SetCursorPosition(30, 25);
                        Console.Write("*****");
                        Console.SetCursorPosition(30, 26);
                        Console.Write("*   *");
                        Console.SetCursorPosition(30, 27);
                        Console.Write("*   *");
                        Console.SetCursorPosition(30, 28);
                        Console.Write("*   *");
                        Console.SetCursorPosition(30, 29);
                        Console.Write("*   *");
                        Console.SetCursorPosition(30, 30);
                        Console.Write("*   *");
                        Console.SetCursorPosition(30, 31);
                        Console.Write("*****");
                    }
                }

              
                System.Threading.Thread.Sleep(1000);
                Console.Clear();
                second1 = second1 + 1;
            }
        }   
    }
}
