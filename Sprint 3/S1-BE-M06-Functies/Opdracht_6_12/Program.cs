using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_6_12
{
    class Program
    {
        //kijkt naar de jaar  en zegt of eht een schrikkel jaar is
        static bool IsSchrikkeljaar(int year)
        {

            bool schrikkeljaar;
            if (DateTime.IsLeapYear(year))
            {
                schrikkeljaar = true;
            }
            else
            {
                schrikkeljaar = false;
            }
            return (schrikkeljaar);
        }

        static void Main(string[] args)
        {

            Console.WriteLine("Voer een jaar in: ");
            int jaar = Convert.ToInt32(Console.ReadLine());

            if (IsSchrikkeljaar(jaar) == true)
            {
                Console.WriteLine("{0} is een schrikeljaar", jaar);
            }
            else
            {
                Console.WriteLine("{0} is geen schrikeljaar", jaar);
            }
            Console.ReadKey();
        }
    }

}
