using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_6_4
{
    class Program
    {
        
            static void Main(string[] args)
        {
            string symbool = "";
            int antwoord = 0;

            Console.WriteLine("Voer getal 1 in:");
            int getal1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Voer een reken symbool in:");
            symbool =  Console.ReadLine();
            Console.WriteLine("Voer getal 2 in:");
            int getal2 = Convert.ToInt32(Console.ReadLine());

            switch (symbool)
            {
                case "*":
                    antwoord = getal1 * getal2; 
                    break;
                case "/":
                    antwoord = getal1 / getal2; 
                    break;
                case "-":
                    antwoord = getal1 - getal2; 
                    break;
                case "+":
                    antwoord = getal1 + getal2; 
                    break;
                case "%":
                    antwoord = getal1 % getal2; 


                    break;
                default:
                    Console.WriteLine("je kan alleen * / - + % invullen");
                    break;
            }
            
            Console.WriteLine("het antwoord is " + antwoord);
            Console.ReadKey();
            
        }
    }
}
