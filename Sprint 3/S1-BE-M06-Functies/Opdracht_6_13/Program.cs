//Made by RazenGear (Vincent Gunnink)
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;


namespace Opdracht_6_13
{
    class Program
    {
        static void Main(string[] args)
        {
            string retry; // Defines a string called retry.

            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Animal Sound Player");
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("Enter one of the next options.");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("1. Cat    | 2. Dog | 3. Donkey | 4. Frog  | 5. Goat");
                Console.WriteLine("6. Monkey | 7. Owl | 8. Pig    | 9. Tiger | 10. Wolf");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("If you type a number above 10 a random sound will be played.\n");
                Console.ForegroundColor = ConsoleColor.Green;

                string inputString = Console.ReadLine();

                if (int.TryParse(inputString, out int result)) //Check if the users input is a number.
                {
                    int input = Convert.ToInt32(inputString);

                    if (input <= 32767)
                    {
                        Console.Clear();
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        if (input > 10)
                        {
                            Console.WriteLine($"Now playing a random sound.");
                        }
                        else
                        {
                            Console.WriteLine($"Now playing {input}.");
                        }

                        if (PlayAnimalSound(input) == "!")
                        {
                            Console.Clear();
                            SoundPlayer REEE = new SoundPlayer(Environment.CurrentDirectory + "\\sounds\\REEE.wav");
                            REEE.Play();
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine("How about you DON'T enter negative numbers >_<");
                            Console.ForegroundColor = ConsoleColor.Gray;
                        }
                        else
                        {
                            SoundPlayer animalSound = new SoundPlayer(PlayAnimalSound(input));
                            animalSound.Play();
                        }
                    }
                    else
                    {
                        Console.Clear();
                        //Tells the user to only type numbers under 32767.
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("Please only enter numbers under 32767.");
                        Console.ForegroundColor = ConsoleColor.Gray;
                    }
                }
                else
                {
                    Console.Clear();
                    //Tells the user to only type numbers.
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Please only enter numbers.");
                    Console.ForegroundColor = ConsoleColor.Gray;
                }

                //Asks if the user wants to try again.
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("\nWould you like to try again? (yes/no)\n");
                retry = Console.ReadLine(); //Saves the users anwser.

                //Asks the user if they want to run the program again and checks if their input was valid.
                while (!(retry.ToLower().Contains("yes") || retry.ToLower().Contains("no")))
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("\nInvalid! answer \'yes\' or \'no\'.\n");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    retry = Console.ReadLine(); //Saves the users input.
                }
            }
            while (retry.ToLower().Contains("yes")); //If the user entered 'yes' the program will restart
        }

        //This function checks if 'input' is a value from 1 to 10.
        //If the value is higher then 11 it will choose a random number between 1 and 10.
        //The function will return a path to a .wav file depended the 'input' value.
        public static string PlayAnimalSound(int input)
        {
            string output = "0";
            Random randomS = new Random();
            
            if (input > 10)
            {
                input = randomS.Next(1, 11);
            }

            switch (input) 
            {
                case 1:
                    output = Environment.CurrentDirectory + "\\sounds\\cat.wav";
                    break;
                case 2:
                    output = Environment.CurrentDirectory + "\\sounds\\dog.wav";
                    break;
                case 3:
                    output = Environment.CurrentDirectory + "\\sounds\\donkey.wav";
                    break;
                case 4:
                    output = Environment.CurrentDirectory + "\\sounds\\frog.wav";
                    break;
                case 5:
                    output = Environment.CurrentDirectory + "\\sounds\\goat.wav";
                    break;
                case 6:
                    output = Environment.CurrentDirectory + "\\sounds\\monkey.wav";
                    break;
                case 7:
                    output = Environment.CurrentDirectory + "\\sounds\\owl.wav";
                    break;
                case 8:
                    output = Environment.CurrentDirectory + "\\sounds\\pig.wav";
                    break;
                case 9:
                    output = Environment.CurrentDirectory + "\\sounds\\tiger.wav";
                    break;
                case 10:
                    output = Environment.CurrentDirectory + "\\sounds\\wolf.wav";
                    break;
                default:
                    output = "!";
                    break;
            }

            return output;
        }    
    }
}
