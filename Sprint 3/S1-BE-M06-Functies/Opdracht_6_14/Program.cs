using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_6_14
{
    class Program
    {
        static int BerekenLeeftijd(DateTime datum)
        {
            //Get current date
            DateTime now = DateTime.Now;

            //zet de string om naar een Datetime format
            DateTime Geboorte_Datum = datum;

            //bereken de leeftijd
            TimeSpan leeftijd = now - Geboorte_Datum;
            int age = 0;
            //kijk of het persoon al jarig is geweest of niet
            if (leeftijd.Days > 0 && Geboorte_Datum.Month < now.Month)
            {
                age = now.Year - Geboorte_Datum.Year;
                return (age);

            }
            else if (Geboorte_Datum.Day == now.Day && Geboorte_Datum.Month == now.Month)
            {
                age = now.Year - Geboorte_Datum.Year;
                return (age);
            }
            else
            {
                age = now.Year - Geboorte_Datum.Year;
                return (age - 1);

            }
            
            

        }


        public static void Main(string[] args)
        {
            Console.WriteLine("Dit programme berekent jouwn leeftijd.");
            //vraag voor de geboorte datum
            Console.Write("Voer je geboorte datum in: ");

            //vraag voor de leeftijd
            int leeftijd = BerekenLeeftijd(Convert.ToDateTime(Console.ReadLine()));

            //geef een output
            Console.WriteLine("\nJe bent {0} jaar oud", leeftijd);

            //vraag of de gebruiker het opnieuw wilt uitvoeren
            Console.WriteLine("\nWilt u het opnieuw proberen? (ja/nee)");

            //kijk naar de output van de gebruiker
            if (Console.ReadLine() == "ja")
            {
                //clear de console
                Console.Clear();

                //ga naar het begin
                Main(args);
            }
            
        }
    }
}
