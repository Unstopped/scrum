using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_6_5
{
    class Program
    {

        static void LegeRegels(int getal)
        {
            int tijd = 0;
            while (tijd <= getal)
            {
                tijd = tijd + 1;
                Console.WriteLine("");
            }
        }


        static void Main(string[] args)
        {
            Console.WriteLine("Dit programma laat een hoelveelheid lege regels zien.");
            Console.Write("Voer een getal in: ");
            LegeRegels(Convert.ToInt32(Console.ReadLine()));
        }
    }
}
