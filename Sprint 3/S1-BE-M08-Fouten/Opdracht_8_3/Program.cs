using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_8_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("voer het deeltal in: ");
            int getal1 = Convert.ToInt32(Console.ReadLine());

            Console.Write("voer de deler in: ");
            int getal2 = Convert.ToInt32(Console.ReadLine());

           
            try
            {
                int number = getal1 / getal2;
                Console.WriteLine("\nHet antwoord is: " + getal1 / getal2);
            }
            catch (Exception e)
            {
                Console.WriteLine("\nWie deelt door nul is een snul!");
            }

        }
    }
}
