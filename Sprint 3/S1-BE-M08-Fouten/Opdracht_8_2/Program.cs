using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_8_2
{

    class Program
    {
        static void Main(string[] args)
        {
            string strDatum;

            Console.WriteLine("vul de datum in dd-mm-yyyy: ");
            strDatum = Console.ReadLine();

            while (!(DateTime.TryParse(strDatum, out DateTime result)))
            {
                Console.WriteLine("deze datum klopt niet vul een nieuwe in dd-mm-yyyy: ");
                strDatum = Console.ReadLine();
            }


            DateTime datum = Convert.ToDateTime(strDatum);

            int days = Convert.ToInt32(System.DateTime.DaysInMonth(datum.Year, datum.Month));


            Console.WriteLine("in de maand {0} zitten {1} dagen.", datum.ToString("MMMM", new System.Globalization.CultureInfo("nl-NL")), days);
            Console.ReadLine();
            
           
        }
    }
}