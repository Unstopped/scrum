using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_7_10
{
    class Program
    {
        static void Main(string[] args)
        {
            //creates a list for the names.
            string[] namen = new string[6];

            double[,] cijfers = new double[,]
            {
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
            };

            double[,] gemiddelde = new double[,]
           {
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
           };

            //declares a string called naam.
            string naam = "";

            //
            bool okPressed1 = false;

            //Asks for all your names and adds the names to a list untill you type "OK"
            Console.WriteLine("Wat zijn jullie namen?");

            for (int i = 0; i < (namen.Length); i++)
            {
                    naam = Console.ReadLine();
                    namen[i] = naam;
            }

            //For loop to add all maths grades.
            for (int i = 0; i < (namen.Length); i++)
            {
                Console.Clear();
                Console.WriteLine("Wat is {0} zijn cijfer voor Rekenen?", namen[i]);
                cijfers[0, i] = Convert.ToDouble(Console.ReadLine());
                gemiddelde[0, i] = gemiddelde[0, i] + Convert.ToDouble(cijfers[0, i]);
            }

            //For loop to add all dutch grades.
            for (int i = 0; i < (namen.Length); i++)
            {
                Console.Clear();
                Console.WriteLine("Wat is {0} zijn cijfer voor Nederlands?", namen[i]);
                cijfers[1, i] = Convert.ToDouble(Console.ReadLine());
                gemiddelde[1, i] = gemiddelde[1, i] + Convert.ToDouble(cijfers[1, i]);
            }

            //For loop to add all english grades.
            for (int i = 0; i < (namen.Length); i++)
            {
                Console.Clear();
                Console.WriteLine("Wat is {0} zijn cijfer voor Engels?", namen[i]);
                cijfers[2, i] = Convert.ToDouble(Console.ReadLine());
                gemiddelde[2, i] = gemiddelde[2, i] + Convert.ToDouble(cijfers[2, i]);
                Console.Clear();
            }

            //Displays the report of everyone you gave up.
            for (int i = 0; i < (namen.Length); i++)
            {
                //Grade for math.
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Rapport van {0}:", namen[i]);
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("Cijfer voor Rekenen: ");

                if (cijfers[0, i] < 5 || cijfers[2, i] == 5)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                }

                Console.Write(cijfers[0, i] + "\n");
                Console.ForegroundColor = ConsoleColor.White;

                //grade for Dutch.
                Console.Write("Cijfer voor Nederlands: ");

                if (cijfers[1, i] < 5 || cijfers[2, i] == 5)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                }

                Console.Write(cijfers[1, i] + "\n");
                Console.ForegroundColor = ConsoleColor.White;

                //grade for English.
                Console.Write("Cijfer voor Engels: ");

                if (cijfers[2, i] < 5 || cijfers[2, i] == 5)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                }

                Console.Write(cijfers[2, i] + "\n");
                Console.ForegroundColor = ConsoleColor.White;

                //Average grade.
                Console.Write("Gemiddelde: ");
                gemiddelde[0, i] = (cijfers[0, i] + cijfers[1, i] + cijfers[2, i]) / 3;

                if (gemiddelde[0, i] < 5 || gemiddelde[0, i] == 5)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                }

                Console.Write(gemiddelde[0, i] + "\n");
                Console.WriteLine("");
            }
            Console.ReadLine();
        }
    }
}
