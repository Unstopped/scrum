using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_7_9
{
    class Program
    {
        static void Main(string[] args)
        {
            //0 = voornaam
            //1 = achternaam
            //2 = geboortdatum
            //3 = studentenummer
            //4 = geboorteplaats
            // ... Create 2D array of strings.
            string[,] array = new string[,]
            {
            {"Syon", "Foppen", "10 maart 2002", "0308676", "Assen"},
            {"Erel", "Cetin", "8 augustus 2001", "0302077", "Enschede"},
            {"Job", "Van der Sluis", "24 maart 2002", "0311932", "Enschede"},
            {"Rico", "Boerdam", "30 april 2002", "0314452", "Brumme"},
            {"Vincent", "Gunnink", "24 juli 2001", "0308612", "Enschede"}
            };

            // ... Print out values.
            Console.WriteLine("{0} {1} ({2}) is geboren op {3} in {4}", array[0,0], array[0, 1], array[0, 3], array[0, 2], array[0, 4]);
            Console.WriteLine("{0} {1} ({2}) is geboren op {3} in {4}", array[1, 0], array[1, 1], array[1, 3], array[1, 2], array[1, 4]);
            Console.WriteLine("{0} {1} ({2}) is geboren op {3} in {4}", array[2, 0], array[2, 1], array[2, 3], array[2, 2], array[2, 4]);
            Console.WriteLine("{0} {1} ({2}) is geboren op {3} in {4}", array[3, 0], array[3, 1], array[3, 3], array[3, 2], array[3, 4]);
            Console.WriteLine("{0} {1} ({2}) is geboren op {3} in {4}", array[4, 0], array[4, 1], array[4, 3], array[4, 2], array[4, 4]);
        }
    }
}
