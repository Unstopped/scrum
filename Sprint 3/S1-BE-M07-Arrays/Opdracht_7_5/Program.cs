// gemaakt door erel

using System;
using System.Collections.Generic;

namespace Opdracht_7_5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Dit programma kijkt of het door u ingevoerde BSN nummer geldig is");
            Console.Write("Voer een BSN nummer in: ");
            // lees de input van de gebruiker
            string str = Console.ReadLine();
            int g = 9;
            int totaal = 0;
            double j = 0;
            //maak een list aan
            List<int> getallen = new List<int>();
            // een for lus om alle chars in een int list zetten
            for (int i = 0; i < str.Length; i++)
            {
                j = char.GetNumericValue(str[i]);
                getallen.Add(Convert.ToInt32(j));
            }
            //elf berekening uit voeren
            for (int i = 0; i < str.Length; i++)
            {
                if (i == 8)
                {
                    totaal = totaal + getallen[i] * -1;
                }
                else
                {
                    totaal = totaal + getallen[i] * g;
                }
            g--;        
            }
            //kijken of het gedeeld door 11 een heel getal is zo ja is het geldig
            if(totaal %11 == 0)
            {
                Console.WriteLine("\nHet bsn nummer is geldig!");
            }
            else
            {
                Console.WriteLine("\nHet bsn nummer is niet geldig!");
            }
            Console.ReadKey();
        }
    }
}

