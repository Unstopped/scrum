using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_7_4
{
    class Program
    {
        static void Main(string[] args)
        {
            //vraag hoe lang het wachtwoord meot zijn
            Console.Write("Voer hier de lengte in van het gewenste wachtwoord: ");
            int lengte = Convert.ToInt32(Console.ReadLine());

            //kijk of het wachtwoord niet te kort of te lang is
            if ((lengte <= 60) && (lengte >= 10))
             {
                //zet alle letters, cijfers en tekens in een array
                char[] alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*".ToCharArray();

                //zeg de varibels
                int counter = 0;
                string password = "";
                Random randm = new Random();

                //zorg dat het wachtwoord de gewenste lengte krijgt
                while (counter < lengte)
                {
                    counter = counter + 1;
                    //voeg de karaters toe aan het wachtwoord
                    password = password + alpha[randm.Next(1, 70) - 1];
                }
                //geef het wachtwoord weer
                Console.WriteLine("\nHet wachtwoord is: " + password);
                Console.ReadKey();
            }
            else
            {
                if (lengte <= 10)
                {
                    Console.Clear();
                    Console.WriteLine("Het wachtwoord moet langer zijn dan 10 karakters!");
                    Console.WriteLine("Druk op enter om verder te gaan....");
                    Console.ReadKey();
                    Console.Clear();
                    Main(args);
                }
                else if (lengte >= 60)
                {
                    Console.Clear();
                    Console.WriteLine("Het wachtwoord moet kleiner zijn dan 60 karakters!");
                    Console.WriteLine("Druk op enter om verder te gaan....");
                    Console.ReadKey();
                    Console.Clear();
                    Main(args);
                }

            }

           
            
        }
    }
}
