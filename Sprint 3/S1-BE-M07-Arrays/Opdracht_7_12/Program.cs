using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_7_12
{
    class Program
    {
        static int key;

        static string Caesar(string value, int shift)
        {
            char[] buffer = value.ToCharArray();
            for (int i = 0; i < buffer.Length; i++)
            {
                // Letter.
                char letter = buffer[i];
                // Add shift to all.
                letter = (char)(letter + shift);
                // Subtract 26 on overflow.
                // Add 26 on underflow.
                if (letter > 'Z')
                {
                    letter = (char)(letter - key);
                }
                else if (letter < 'A')
                {
                    letter = (char)(letter + key);
                }
                // Store.
                buffer[i] = letter;
            }
            return new string(buffer);
        }

        static void Main(string[] args)
        {
            for (bool i; i = true;)
            {
                Console.WriteLine("Voer een text in. (Alleen hoofdletters)");
                string a = Console.ReadLine();
                Console.Clear();

                if (a == a.ToUpper())
                {
                    Console.WriteLine("Voer een key tussen 1 en 26 in.");
                    key = Convert.ToInt32(Console.ReadLine());
                    Console.Clear();

                    if (key < 1 || key > 26)
                    {
                        Console.WriteLine("Dat is niet een key tussen 1 en 26.");
                        System.Threading.Thread.Sleep(2000);
                        Console.Clear();
                    }

                    else
                    {
                        string b = Caesar(a, 18); // Ok
                        string c = Caesar(b, -18); // Ok

                        Console.WriteLine("Ingevoerde data: {0}", a);
                        Console.WriteLine("Versleutelde data: {0}", b);
                        Console.WriteLine("Ontsleutelde data: {0}", a);
                        System.Threading.Thread.Sleep(5000);
                        i = false;
                        Console.Clear();
                    }
                }

                else
                {
                    Console.WriteLine("Die text bestaat niet uit hoofdletters.");
                    System.Threading.Thread.Sleep(2000);
                    Console.Clear();
                }
            }
        }
    }
}
