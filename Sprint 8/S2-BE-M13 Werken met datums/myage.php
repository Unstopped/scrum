<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Opdracht 1</title>
    <style>
      .age {
        position: fixed;
        top: 50%;
        left: 50%;
        /* bring your own prefixes */
        transform: translate(-50%, -50%);
        width: 25%;
        padding: 10px;
        border-radius: 15px;
        background-color: yellow;
        text-align: center;
        border-width: 5px;
        border-color: red;
        border-style: solid;
        font-family: "Palatino Linotype", "Book Antiqua", "Palatino", "serif";
        font-size: 18pt;
      }
    </style>
  </head>
  <body>
    <?php
      $now = new Datetime();
      $result = $now->format('y-m-d');
      $current_date = strtotime($result);
      $old_date = strtotime('2002-03-10');
      $diffrence = $current_date - $old_date;
      $minuten = floor($diffrence/(60));
      $hours = floor($diffrence/(60*60));
      $days = floor($diffrence/(60*60*24));
      $seconds = $diffrence;
      $weeks = ceil($days / 7);
      $years = floor($weeks / 52) -1;
      $months = ceil($years * 12);

      echo "<div class='age'>
      Maanden: $months <br>
      Dagen: $days <br>
      Weken: $weeks <br>
      Uren: $hours <br>
      Minutes: $minuten<br>
      Seconden: $seconds<br>
      </div>"
     ?>
  </body>
</html>
