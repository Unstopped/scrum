<!DOCTYPE html>
<html lang="nl">
  <head>
    <meta charset="utf-8">
    <title>Opdracht 2</title>
    <style>
      .datum {
        position: fixed;
        top: 50%;
        left: 50%;
        /* bring your own prefixes */
        transform: translate(-50%, -50%);
        width: 25%;
        padding: 10px;
        border-radius: 15px;
        background-color: yellow;
        text-align: center;
        border-width: 5px;
        border-color: red;
        border-style: solid;
        font-family: "Palatino Linotype", "Book Antiqua", "Palatino", "serif";
        font-size: 18pt;
      }
    </style>
  </head>
  <body>
    <div class="datum">
      <form method="post">
        Kies de eerste datum <br>
        <input type="date" name="date1"> <br>
        Kies de tweede datum <br>
        <input type="date" name="date2"> <br>
        <input type="submit" name="submit" value="Bereken">
      </form>
      <?php
        if (isset($_POST["submit"])){
          $date1= strtotime($_POST['date1']);
          $date2 = strtotime($_POST['date2']);
          $diffrence = $date2 - $date1;
          $days = floor($diffrence/(60*60*24));

          echo "<br>Het verschill in dagen is: $days";

        }

       ?>
    </div>
  </body>
</html>
