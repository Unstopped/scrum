<!DOCTYPE html>
<html lang="nl" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>do-while-lus</title>
  </head>
  <body>
      <h3>Voorbeeld van de do-while-lus</h3>
        <?php
          echo "<br>Reken het saldo uit zolang saldo lager dan 2000 is";
          $saldo = 100;
          $rente = 0.1;
          $maand = 1;
          echo "<br>Beginsaldo is: $saldo";
          echo "<br>START...";


          do
          {
            if ($saldo >= 2000) {
              echo "<br>Maximale saldo 2000 is bereikt";
            }

            if ($maand == 6 && $saldo < 1000) {
              echo "<br>Je saldo is te laag";
              $saldo = 2001;
            }

            else {
              $saldo = $saldo + ($saldo * $rente);
              $saldo = sprintf("%0.2f", $saldo);
              echo "<br>Maand: $maand je saldo is: $saldo";
              $maand++;

              if ($maand == 2) {
                echo "<br>Februari betaalt geen rente";
              }
            }

          } while ($saldo < 2000);
          echo "<br>FINISH";
         ?>
  </body>
</html>
