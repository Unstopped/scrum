SELECT        TOP (100) PERCENT dbo.tblAuthor.AuthorName, dbo.tblDoctor.DoctorName, dbo.tblEpisode.Title, dbo.tblEpisode.EpisodeDate
FROM            dbo.tblAuthor INNER JOIN
                         dbo.tblEpisode ON dbo.tblAuthor.AuthorId = dbo.tblEpisode.AuthorId INNER JOIN
                         dbo.tblDoctor ON dbo.tblEpisode.DoctorId = dbo.tblDoctor.DoctorId
WHERE        (dbo.tblEpisode.Title LIKE 'H%')