<html>
  <table border='1'>
    <caption>
      <strong>Temopratuur</strong>
    </caption>
    <thead>
      <tr>
        <th>celsius</th><th>fahrenheit</th><th>kelvin</th>

    </thead>
    <tbody>
      <?php
      $teller = -50;

      while ($teller <= 50)
      {
        $celsius = $teller;
        $fahrenheit = $teller * 1.8 + 32;
        $kelvin = $teller +  273.15;

        if ($teller < 0)
        {
          echo "
          <tr bgcolor='#0037ff'>
            <td>$celsius</td><td>$fahrenheit</td><td>$kelvin</td>
          </tr>";
        }
        if ($teller == 0)
        {
          echo "
          <tr>
            <td>$celsius</td><td>$fahrenheit</td><td>$kelvin</td>
          </tr>";
        }

        if ($teller > 0)
        {
          echo "
          <tr bgcolor='#00ff04'>
            <td>$celsius</td><td>$fahrenheit</td><td>$kelvin</td>
          </tr>";
        }
        $teller = $teller + 1;
      }
       ?>
    </tbody>
  </table>
</html>
