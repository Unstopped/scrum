<?php

function getFib($n)
{
    return round(pow((sqrt(5)+1)/2, $n) / sqrt(5));
}

$teller = 0;

while ($teller < 20)
{
  $getal = getFib($teller) . ";";
  echo $getal;
  $teller = $teller + 1;
}

?>
