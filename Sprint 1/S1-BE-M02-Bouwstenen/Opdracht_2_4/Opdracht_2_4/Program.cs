using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht_2_4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Get current date
            DateTime now = DateTime.Now;

            //ask for the birthdate
            Console.Write("Voer je geboorte datum in: ");

            //zet de string om naar een Datetime format
            DateTime Geboorte_Datum = DateTime.Parse(Console.ReadLine());

            //bereken de leeftijd
            TimeSpan leeftijd = now - Geboorte_Datum;

            //kijk of het persoon al jarig is geweest of niet
            if (leeftijd.Days > 0 && Geboorte_Datum.Month < now.Month)
            {
                int age = now.Year - Geboorte_Datum.Year;
                Console.WriteLine("\nJe bent {0} jaar oud", age);
                
            }
            else if (Geboorte_Datum.Day == now.Day && Geboorte_Datum.Month == now.Month)
            {
                Console.WriteLine("\nHappy Birthday!");
            }
            else
            {
                int age = now.Year - Geboorte_Datum.Year;
                Console.WriteLine(age - 1);
            }
        }
    }
}
