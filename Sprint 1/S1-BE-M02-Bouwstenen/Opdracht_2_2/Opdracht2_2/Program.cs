using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht2_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int weight;                                         //Defines a variable to store Weight in.
            double height;                                      //Defines a variable to store Height in.

            Console.WriteLine("Please enter weight:");          //Displays line with text.
            weight = Convert.ToInt32(Console.ReadLine());       //Gets a value from user and converts it to Int32.
            Console.Clear();                                    //Clears the text from the console.

            Console.WriteLine("Please enter height:");          //Displays line with text.
            height = Convert.ToDouble(Console.ReadLine());      //Gets a value from user and converts it to Int32.
            Console.Clear();                                    //Clears the text from the console.

            Console.WriteLine("Your BMI is: ");                 //Displays line with text.
            Console.Write(weight / height / height);            //Adds text to line (Divides Weight by Height and Height).
            Console.ReadKey();
        }
    }
}
