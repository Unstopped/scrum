SELECT 
COUNT(dbo.tblEvent.EventID) AS 'Number of events', 
MAX(dbo.tblEvent.EventDate) AS 'Last date', 
MIN(dbo.tblEvent.EventDate) AS 'First date'
FROM 
dbo.tblEvent